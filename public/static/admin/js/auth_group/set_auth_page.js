require(['icheck'], function(icheck){

	selectParentsAuth = function(){
		// 选择父规则时，子规则跟随
		$(".index-chkbox").on('ifChecked', function(event){
			var self = $(this),
				index = self.parents(".index");
		  	index.find(":checkbox").iCheck('check');
		});
		$(".index-chkbox").on('ifUnchecked', function(event){
	  		var self = $(this),
	  			index = self.parents(".index");
	  	  	index.find(":checkbox").iCheck('uncheck');
		});

	}


	// 单选框复选框样式事件
	iCheckStyle('select-all','auth_id[]');

	// 选择逻辑
	selectParentsAuth();
});