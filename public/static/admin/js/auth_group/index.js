require(['layer','icheck'], function(layer,icheck){

	// 添加用户组
	addGroup = function(){
		$('.add-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");

			layer.open({
			    type: 2,
			    title: '添加用户组',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['60%','60%'],
			    content: url,
			    btn: ['添加','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];
					//调用子页面方法
					var addData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, addData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload();
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			});

		});
	}

	// 编辑用户组
	editGroup = function(){
		$('.edit-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");
			id       = self.attr("data-id");

			layer.open({
			    type: 2,
			    title: '编辑用户信息',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['60%','60%'],
			    content: url+'?id='+id,
			    btn: ['修改','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];

					var editData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, editData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload();
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			}); 

		});
	}


	// 删除
	delGroup = function() {
		$('.delete-btn').click(function(){
			var self = $(this),
				id   = self.attr('data-id'),
				url  = self.attr('data-url');
			layer.msg('确定要删除吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
						var params = {id: id};
						$.post(url, params, function(json){
							if (json.code) {
								layer.alert(json.msg,{icon: 1});
								self.parent().parent().fadeOut('slow');
							} else {
								layer.alert(json.msg,{icon: 2});
							}
						}, 'json');
				}
			});
		});
	};


	// 批量处理
	batchProc = function(){
		$('.batch-btn').click(function(){
			var self 	 = $(this),
				url      = self.attr('data-url'),
				type     = self.attr('data-type'),
				title    = self.attr('data-title');
			var ids 	 = get_checkbox_group_value('release_id[]');

			if (ids == false) {
				layer.alert('请选择要批量'+ title +'的用户',{icon: 2});
				return false;
			}

			layer.alert('确定要批量'+ title +'选中用户吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
						var data = {ids: ids, type:type};
						$.post(url, data, function(json){
							if (json.code) {
								layer.alert(json.msg,{icon: 1});
								pageReload(1000);
							} else {
								layer.alert(json.msg,{icon: 5});
							}
						}, 'json');
				}
			});
		})
	}

	// 重置
	reset = function() {
		$('.btn-reset').click(function(){
			var form = $('#search-form');
			form.find('option').each(function(){
				if ($(this).attr("selected")) {
					$(this).removeProp("selected");
				}
			});

			form.find('[name="title"]').val('');
		});
	};

	// 配置规则/用户
	setAuthOrUser = function() {
		$('.set-btn').click(function(){
			var set_url  = $(this).attr('set-url');
			var save_url = $(this).attr('save-url');
			var title    = $(this).attr('title');

			layer.open({
			  	type: 2,
			  	title: title,
			  	shade: 0.5,
			  	scrollbar : false,
			  	area : ['80%','100%'],
			  	content: set_url,
			  	maxmin: true,
			  	btn: ['保存','取消'],
			  	yes: function (index, frame) {
			  		var iframe = layer.getChildFrame('body', index);
			  		var form = $(iframe).find("#set");

			  		var formData = form.serialize();

			  		$.post(save_url,formData,function(json){
			  			// layer.close(index);
			  			if (json.code) {
			  				layer.alert(json.msg,{icon: 1});
			  				pageReload(500);
			  			}else{
			  				layer.alert(json.msg,{icon: 5});
			  			}
			  		},"json");
			  	}
			});
		});
	}


	// 单选框复选框样式事件
	iCheckStyle('select_all','release_id[]');

	// 添加用户组
	addGroup();

	// 编辑用户组
	editGroup();

	// 删除
	delGroup();

	// 批量处理
	batchProc();

	// 重置
	reset();

	// 配置规则/用户
	setAuthOrUser();

});

