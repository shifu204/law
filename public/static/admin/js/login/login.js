require(['layer','icheck'], function(layer,icheck){

	//单选框样式
	radioStyle = function(){
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' 
		});
	}


	

	ajaxSubmitFrom3 = function(){

		$(document).keydown(function(event){
			if(event.keyCode==13){
				_login_proc();
			}
		});

		$('.submit-ajax').click(_login_proc);
	}



	_login_proc = function () {
		if (_check_form()) {
			form = $('#loginForm');
			url        = form.attr('action');

			formData   = form.serialize();

			$.post(url,formData,function(json) {
				if (json.code) {
					layer.msg(json.msg,{icon: 1});
					goLocation(json.url,1000);
				} else {
					layer.alert(json.msg,{icon: 5});
					$('.captcha').click();
				}

			},"json");

		}
	}



	_check_form = function(){
		if($(".username").val() == ''){
			layer.alert('请输入后台帐号',{icon: 2});
			return false;
		}
		if($(".password").val() == ''){
			layer.alert('请输入密码',{icon: 2});
			return false;
		}
		if($(".verify").val() == ''){
			layer.alert('请输验证码',{icon: 2});
			return false;
		}
		return true;
	}

  
	//单选框样式
	radioStyle();

	ajaxSubmitFrom3();

});

