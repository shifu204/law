require(['layer'], function(){

	// 登出操作
	logout = function() {
		$('.logout-btn').click(function(){
			var self = $(this);
			url = self.attr('data-url');
		    layer.confirm('您确定要退出登录？',{title:'登出提醒'},function (index) {
		        location.href = url;
		    })
		});
	}

	// 清除缓存
	clearCache = function(){
		$('.clear-cache').click(function(){
			var self = $(this);
			url = self.attr('data-url');
		    $.get(url,'',function(json) {
	      		if (json.code) {
	      			layer.alert(json.msg,{icon: 1});
	      			pageReload(1500);
	      		} else {
	      			layer.alert(json.msg,{icon: 5});//图标5是为了区分js的报错
	      		}

	      	},"json");
		})
	}



	// 清除缓存
	clearCache();

	// 登出操作
	logout();
});