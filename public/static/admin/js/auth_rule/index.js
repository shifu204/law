require(['layer',], function(layer){

	// 添加权限
	addRule = function(){
		$('.add-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");

			layer.open({
			    type: 2,
			    title: '添加权限',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['60%','60%'],
			    content: url,
			    btn: ['添加','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];
					//调用子页面方法
					var addData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, addData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload();
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			});

		});
	}

	// 编辑权限
	editRule = function(){
		$('.edit-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");
			id       = self.attr("data-id");

			layer.open({
			    type: 2,
			    title: '编辑权限信息',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['60%','60%'],
			    content: url+'?id='+id,
			    btn: ['修改','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];
					//调用子页面方法
					var editData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, editData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload();
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			}); 

		});
	}


	// 删除
	delRule = function() {
		$('.delete-btn').click(function(){
			var self = $(this),
				id   = self.attr('data-id'),
				url  = self.attr('data-url');
			layer.msg('确定要删除吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
						var params = {id: id};
						$.post(url, params, function(json){
							if (json.code) {
								layer.alert(json.msg,{icon: 1});
								self.parent().parent().parent().parent().fadeOut('slow');
							} else {
								layer.alert(json.msg,{icon: 2});
							}
						}, 'json');
				}
			});
		});
	};

	// // 全部展开
	// expandAll = function(){
	// 	$('.expand-all').click(function(){
	// 		alert(1);
	// 	});

	// }

	// // 全部折叠
	// collapseAll = function(){
	// 	$('.collapse-all').click(function(){
	// 		alert(2);
	// 	});

	// }

	// 添加权限
	addRule();

	// 编辑权限
	editRule();

	// 删除权限
	delRule();

	// // 全部展开
	// expandAll();

	// // 全部折叠
	// collapseAll();
});

