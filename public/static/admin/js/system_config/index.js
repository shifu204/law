require(['layer'], function(layer	){

	// 支付测试
	payTest = function (){
		$('.test-btn').click(function() {
			var self = $(this),
			url       = self.attr("data-url");

	  		$.get(url,'', function(json){
	  			if(json.code){
	  				// 显示支付二维码
	  				layer.open({
	  				  type: 1,
	  				  title: '支付测试',
	  				  closeBtn: 0,
	  				  offset: '100px',
	  				  shadeClose: true,
	  				  content: json.msg.qrcode_url
	  				});
	  				// 记录生成的订单号
	  				$('.order-sn').val(json.msg.order_sn);

	  				// 检测支付是否成功
	  				_checkWxpayFinish();
	  			} else {
	  				layer.alert(json.msg,{icon: 2});
	  			}
	  		});

		});

	}

	//检测微信支付结果
	_checkWxpayFinish = function() {
		var order_sn = $('.order-sn').val();
		if (!order_sn) {
			return false;
		}
		var check_url = $('.order-sn').attr('check-url');

		$.post(check_url, {order_sn: order_sn}, function(json){
			if (json.code == 1) {
				layer.alert(json.msg,{icon: 1});
				pageReload(1000);
			} else { // 支付进行中
				setTimeout(function(){
					_checkWxpayFinish();
				}, 2000);
			}
		}, 'JSON');
	};

	// ajax 方式提交
	ajaxSubmitFrom();

	// 支付测试
	payTest();


});

