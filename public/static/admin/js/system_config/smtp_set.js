require(['layer','icheck'], function(layer,icheck){

	// icheck 风格
	icheck_style = function(){
		$('input').iCheck({
			radioClass: 'iradio_flat-blue'
		});
	}


	// 发送邮件
	sendMail = function(){
		$('.send-btn').click(function() {
			var self     = $(this),
			url          = self.attr("data-url");
			var mailbox  = $(".mailbox-test").val();

			sendData = {'mailbox':mailbox};

			$.post(url, sendData, function(json){
				if(json.status){
					layer.alert(json.info,{icon: 1});
				} else {
					layer.alert(json.info,{icon: 2});
				}
			});

		});
	}


	// ajax 方式提交
	ajaxSubmitFrom();

	// icheck 风格
	icheck_style();

	// 发送邮件
	sendMail();
});

