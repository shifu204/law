require(['layer','icheck','ajaxfileupload'], function(layer,icheck,ajaxfileupload){

	// 添加
	add = function(){
		$('.add-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");

			layer.open({
			    type: 2,
			    title: '添加资讯',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['80%','80%'],
			    content: url,
			    btn: ['添加','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];
					//调用子页面方法
					var addData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, addData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload(2000);
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			});

		});
	}

	// 编辑
	edit = function(){
		$('.edit-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");
			id       = self.attr("data-id");

			layer.open({
			    type: 2,
			    title: '编辑资讯信息',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['80%','80%'],
			    content: url+'?id='+id,
			    btn: ['修改','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];
					//调用子页面方法
					var editData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, editData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload(2000);
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			}); 

		});
	}

	// 选择图片
	selectImg = function() {
		$('.img-thumbnail').click(function(event) {
			$(this).next(".uploda-image").click();
		});
	};

	// 上传图片
	uploadImg = function() {
		$('.image-td').on("change",".uploda-image",function(){
			var uploda = $(this),
			    url = uploda.attr('data-url'),
			    fileElementId = uploda.attr('id'),
				id  = uploda.attr('data-id'),
				img = uploda.prev('.img-thumbnail');
			var path = 'article';

				// alert(url);
			$.ajaxFileUpload({
				url : url,
				secureuri:false,
				fileElementId : fileElementId,
				dataType: 'json',
				data:{id: id,path: path},
				success: function (json) {
					if (json.code) {
						layer.alert(json.msg,{icon: 1});
						img.prop('src', json.url);
						// pageReload(1000);
					} else {
						layer.alert(json.msg,{icon: 2});
					}
				},
				error: function (data, status, e) {
					alert(e);
				}
			});
		})
	};


	// 删除
	del = function() {
		$('.delete-btn').click(function(){
			var self = $(this),
				id   = self.attr('data-id'),
				url  = self.attr('data-url');
			layer.msg('确定要删除吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
						var params = {id: id};
						$.post(url, params, function(json){
							if (json.code) {
								layer.alert(json.msg,{icon: 1});
								self.parent().parent().fadeOut('slow');
							} else {
								layer.alert(json.msg,{icon: 2});
							}
						}, 'json');
				}
			});
		});
	};

	// 批量处理
	batchProc = function(){
		$('.batch-btn').click(function(){
			var self 	 = $(this),
				url      = self.attr('data-url'),
				type     = self.attr('data-type'),
				title    = self.attr('data-title');
			var ids 	 = get_checkbox_group_value('release_id[]');

			if (ids == false) {
				layer.alert('请选择要批量'+ title +'的资讯',{icon: 2});
				return false;
			}

			layer.alert('确定要批量'+ title +'选中资讯吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
						var data = {ids: ids, type:type};
						$.post(url, data, function(json){
							if (json.code) {
								layer.alert(json.msg,{icon: 1});
								pageReload(1000);
							} else {
								layer.alert(json.msg,{icon: 5});
							}
						}, 'json');
				}
			});
		})
	}

	// 重置
	reset = function() {
		$('.btn-reset').click(function(){
			var form = $('#search-form');
			form.find('[name="new_title"]').val('');
		});
	};

	
    // 单选框复选框样式事件
	iCheckStyle('select_all','release_id[]');

	// 添加
	add();

	// 编辑
	edit();

	selectImg();

	//上传图片
	uploadImg();

	// 删除
	del();

	//批量处理
	batchProc();

	//重置
	reset();

	


});

