require(['ueditorCN',], function(ueditorCN){

// 添加资讯内容
	addContent = function(){
		//实例化编辑器，配置内容
		var ue = UE.getEditor('content', {
		    toolbars: [
		        [
		            'undo', //撤销
		            'redo', //重做
		            'bold', //加粗
		            'indent', //首行缩进
		            'snapscreen', //截图
		            'italic', //斜体
		            'underline', //下划线
		            'strikethrough', //删除线
		            'subscript', //下标
		            'fontborder', //字符边框
		            'superscript', //上标
		            'formatmatch', //格式刷
		            'source', //源代码
		            'blockquote', //引用
		            'pasteplain', //纯文本粘贴模式
		            'forecolor', //字体颜色
		            'backcolor', //背景色
		        ]
		    ],
		    initialContent:'请添加资讯内容。。。',//初始内容
		    autoClearinitialContent:true,//是否自动清除编辑器初始内容
		    saveInterval:'3000'//自动保存间隔时间，单位ms
		});
	}

	addContent();


});

