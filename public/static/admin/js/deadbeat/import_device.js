require(['layer'], function(layer){

// 点击浏览事件
clickBrowse = function(){
	var form = $('.ajax-form');

	// 点击浏览按钮，启动点击隐藏文件域事件
	form.on('click', '.btn-select-file', function(){
		form.find('#upload_file').click();
	});

	// 点击文件域事件
	form.on('change', '#upload_file', function(){
		var file = form.find('#upload_file').val();
		form.find('.file-fake-path').val(file);
	});
}

// 点击浏览事件
clickBrowse();

});

