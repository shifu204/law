require(['layer','icheck','ajaxfileupload'], function(layer,icheck,ajaxfileupload){

	// 编辑
	edit = function(){
		$('.edit-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");
			id       = self.attr("data-id");

			layer.open({
			    type: 2,
			    title: '编辑老赖信息',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['70%','60%'],
			    content: url+'?id='+id,
			    btn: ['修改','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];
					//调用子页面方法
					var editData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, editData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload(2000);
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			}); 

		});
	}



	// 删除
	del = function() {
		$('.delete-btn').click(function(){
			var self = $(this),
				id   = self.attr('data-id'),
				url  = self.attr('data-url');
			layer.msg('确定要删除吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
						var params = {id: id};
						$.post(url, params, function(json){
							if (json.code) {
								layer.alert(json.msg,{icon: 1});
								self.parent().parent().fadeOut('slow');
							} else {
								layer.alert(json.msg,{icon: 2});
							}
						}, 'json');
				}
			});
		});
	};

	// 批量处理
	batchProc = function(){
		$('.batch-btn').click(function(){
			var self 	 = $(this),
				url      = self.attr('data-url'),
				type     = self.attr('data-type'),
				title    = self.attr('data-title');
			var ids 	 = get_checkbox_group_value('release_id[]');

			if (ids == false) {
				layer.alert('请选择要批量'+ title +'的老赖',{icon: 2});
				return false;
			}

			layer.alert('确定要批量'+ title +'选中老赖吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
						var data = {ids: ids, type:type};
						$.post(url, data, function(json){
							if (json.code) {
								layer.alert(json.msg,{icon: 1});
								pageReload(1000);
							} else {
								layer.alert(json.msg,{icon: 5});
							}
						}, 'json');
				}
			});
		})
	}

	// 批量导出处理
	batchOutput = function(){
		$('.output-btn').click(function(){
			var self 	 = $(this),
				type     = self.attr('data-type'),
				title    = self.attr('data-title');
			var ids 	 = get_checkbox_group_value('release_id[]');

			if (ids == false) {
				layer.alert('请选择要批量'+ title +'的老赖',{icon: 2});
				return false;
			}

			layer.alert('确定要批量'+ title +'选中老赖吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
				    	var index  = 0;
				    	var setIntervalId = setInterval(function(){
				    		if(index < ids.length){
				    			var url = '/admin/deadbeat/out/act/deadbeat/id/'+ids[index]+'.html';
								var a = $('<a style="display: none" id="aa'+index+'" href='+url+'></a>');
								$('body').append(a);
								document.getElementById('aa'+index).click();
								index++;
				    		} else{
				    			clearInterval(setIntervalId)
				    		}
				    		
				    	}, 1000)
				}
			});
		})
	}

	// 重置
	reset = function() {
		$('.btn-reset').click(function(){
			var form = $('#search-form');
			form.find('[name="name"]').val('');
			form.find('[name="card_sn"]').val('');
		});
	};

	// 导入
	importDevice = function(){
		$('.import-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");
			title = self.attr("data-title");
			act = self.attr("data-act");

			layer.open({
			    type: 2,
			    title: '导入' + title,
			    shade: 0.5,
			    scrollbar : false,
			    area : ['50%','50%'],
			    content: url,
			    btn: ['导入','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];

					var file     = $(childBody).find('#upload_file').val();
	
					if (!file) {
						layer.alert('请选择导入文件',{icon: 5});
						return false;
					}

					// 设置act
					$(childBody).find('#act').val(act);

					// 设置上传文件name
					$(childBody).find('#upload_file').attr('name',act);

					$(childBody).find('.ajax-form').submit();

				},
				cancel: function(){ 
					// 点击关闭时刷新页面
				    pageReload(1000);
				},
				btn2: function(index, layero){
				    // 点击关闭时刷新页面
				    pageReload(1000);
				}
			});

		});
	}

	// 详情
	releaseDetails = function(){
		$('.details-btn').click(function() {
			var self = $(this),
			url = self.attr("data-url");
			id  = self.attr("data-id");

			layer.open({
			  type: 2,
			  title: '用户详情',
			  shade: 0.5,
			  scrollbar : false,
			  area : ['90%','90%'],
			  content: url+'?id='+id,
			  btn: ['关闭'],
			});

		});
	}


	// 详情
	releaseDetails();

	// 导入
	importDevice();

	
    // 单选框复选框样式事件
	iCheckStyle('select_all','release_id[]');

	// 编辑
	edit();


	// 删除
	del();

	//批量处理
	batchProc();

	//批量导出处理
	batchOutput();

	//重置
	reset();

	


});

