require(['layer',], function(layer){

	// 添加
	add = function(){
		$('.add-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");

			layer.open({
			    type: 2,
			    title: '添加资讯分类',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['80%','40%'],
			    content: url,
			    btn: ['添加','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];
					//调用子页面方法
					var addData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, addData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload(2000);
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			});

		});
	}

	// 编辑
	edit = function(){
		$('.edit-btn').click(function() {
			var self = $(this),
			url      = self.attr("data-url");
			proc_url = self.attr("proc-url");
			id       = self.attr("data-id");

			layer.open({
			    type: 2,
			    title: '编辑分类信息',
			    shade: 0.5,
			    scrollbar : false,
			    area : ['80%','40%'],
			    content: url+'?id='+id,
			    btn: ['修改','关闭'],
			    yes : function(index, layerDom){
					var childBody = layer.getChildFrame('body', index);
					//iframe窗口
					var iframeWin = window[layerDom.find('iframe')[0]['name']];
					//调用子页面方法
					var editData = $(childBody).find('.ajax-form').serialize();
					//发布
					$.post(proc_url, editData, function(json){
						if(json.code){
							layer.alert(json.msg,{icon: 1});
							layer.close(index);
							pageReload(2000);
						} else {
							layer.alert(json.msg,{icon: 2});
						}
					});
				}
			}); 

		});
	}


	// 删除
	del = function() {
		$('.delete-btn').click(function(){
			var self = $(this),
				id   = self.attr('data-id'),
				url  = self.attr('data-url');
			layer.msg('确定要删除吗？', {
				time: 0 ,//不自动关闭,
				btn: ['确定', '关闭'],
				yes: function(index){
				    layer.close(index);
						var params = {id: id};
						$.post(url, params, function(json){
							if (json.code) {
								layer.alert(json.msg,{icon: 1});
								self.parent().parent().fadeOut('slow');
							} else {
								layer.alert(json.msg,{icon: 2});
							}
						}, 'json');
				}
			});
		});
	};

	


	// 添加
	add();

	// 编辑
	edit();

	// 删除
	del();

	


});

