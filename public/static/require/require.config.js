var require = {
    "baseUrl":  STATIC,
    "paths" : {
        "js" : './js',
        "jquery" : './plugins/jQuery/jquery-2.2.3.min',
		"bootstrap" : './bootstrap/js/bootstrap.min',
        "layer" : './plugins/layer/2.4/layer',
        "datepicker" : './plugins/datepicker/bootstrap-datepicker',
        "datepickerCN" : './plugins/datepicker/locales/bootstrap-datepicker.zh-CN',
        "icheck": './plugins/icheck-1.x/icheck.min',
        "select2": './plugins/select2/select2.full.min',
		"plugins" : './plugins',
		'webuploader' : './plugins/webuploader/webuploader',
		"datatables" : './plugins/datatables/media/js/jquery.dataTables.min',
		"datatables.net" : './plugins/datatables/media/js/dataTables.bootstrap.min',
		"datatables.net-select" : './plugins/datatables/extensions/Select/js/dataTables.select.min',
		"mydatatable" : './require/datatable',
        "dialogBox" : './plugins/dialogBox/js/jquery.dialogBox',
        "bootstrapValidator" : './plugins/bootstrapValidator/bootstrapValidator.min',
        "jquery.form" : './plugins/jquery.form/jquery.form.min',
        "uploadPreview" : './plugins/uploadPreview/uploadPreview.min',
        "ajaxFileUpload" : './plugins/ajaxfileupload/ajaxfileupload',
        "toastr" : './plugins/toastr/toastr',
        "contextMenu" : './plugins/contextmenu/jquery.contextmenu',
        "zTree" : './plugins/zTree/js/jquery.ztree.all',
        "wdTree" : './plugins/wdTree/jquery.tree',
        "croppic" : './plugins/croppic/croppic',
        "cropper" : './plugins/cropper/dist/cropper.min',
        "ajaxfileupload" : './plugins/ajaxfileupload/ajaxfileupload',
        "ueditor.config" : './plugins/ueditor/1.4.3/ueditor.config',
        "ueditor" : './plugins/ueditor/1.4.3/ueditor.all',
        "ueditorCN" : './plugins/ueditor/1.4.3/lang/zh-cn/zh-cn',
        "zeroClipboard" : './plugins/ueditor/1.4.3/third-party/zeroclipboard/ZeroClipboard',
        "cxSelect" : './plugins/cxSelect/jquery.cxselect',
        // "showPicture" : '',
    },
	"shim" : {
		"webuploader" : {
			"deps" : ['jquery', 'css!./plugins/webuploader/webuploader.css']
		},

        "layer" : {
            "deps" : ['jquery', 'css!./plugins/layer/2.4/skin/layer']
        },

        "datepicker" : {
            "deps" : ['jquery', 'css!./plugins/datepicker/datepicker3']
        },

        "datepickerCN" : {
            "deps" : ['datepicker']
        },

        "icheck" : {
            "deps" : ['jquery', 'css!./plugins/icheck-1.x/skins/all']
        },

        "select2" : {
            "deps" : ['jquery', 'css!./plugins/select2/select2.min']
        },

        "datatables.net" : {
            "deps" : ['jquery', 'datatables','css!./plugins/datatables/media/css/dataTables.bootstrap.min', 
					'css!./plugins/datatables/extensions/Select/css/select.dataTables.min']
        },

        "dialogBox" : {
            "deps" : ['css!./plugins/dialogBox/css/jquery.dialogbox']
        },

        "bootstrapValidator" : {
            "deps" : ['css!./plugins/bootstrapValidator/bootstrapValidator.min']
        },

        "toastr" : {
            "deps" : ['css!./plugins/toastr/toastr']
        },

        "contextMenu" : {
            "deps" : ['css!./plugins/contextmenu/jquery.contextmenu']
        },

        "zTree" : {
            "deps" : ['css!./plugins/zTree/css/zTreeStyle/zTreeStyle']
        },

        "wdTree" : {
            "deps" : ['css!./plugins/wdTree/css/tree']
        },

        "croppic" : {
            "deps" : ['jquery','css!./plugins/croppic/assets/css/croppic']
        },

        "cropper" : {
            "deps" : ['jquery','css!./plugins/cropper/dist/cropper.min']
        },

        "ajaxfileupload" : {
            "deps" : ['jquery']
        },

        "ueditor.config" : {
            "deps" : ['jquery']
        },

        "ueditor" : {
            "deps" : ['jquery', 'ueditor.config','css!./plugins/ueditor/1.4.3/themes/default/css/ueditor']
        },

        "ueditorCN" : {
            "deps" : ['ueditor']
        },

        //  "showPicture" : {
        //     "deps" : ['css!./plugins/showPicture/css/sim-prev-anim']
        // },

    },
    "map" : {
        //将所有css的后缀交给css.min.js来处理
        '*' : {
            'css': './require/css.min' // require_css模块的地址路径，以便可以requirecss文件
        }
    },
    "urlArgs" : "version=" + VERSION
};
