//删除左右两端的空格
function trim(str){ 
	return str.replace(/(^\s*)|(\s*$)/g, "");
}

//检查某个字符串是否为空
function isEmpty(str){
	if(typeof(str) == 'undefined'){
		return true;
	}
	if(trim(str) == ''){
		return true;
	}
	return false;
}

/**
 * 全选/反选
 * @param string cls 全选/反选按钮class
 * @param string name 被选择的控件组名字
 */
function select_all(cls, name){
	var selector = $("." + cls);
	var eles = $("[name='"+name+"']");
	$(selector).click(function(){
		if($(selector).is(':checked')){
			$(eles).prop('checked', true);
		} else {
			$(eles).prop('checked', false);
		}
	});	
}

/**
 * iCheck插件单选框复选框样式与事件全选/反选
 * @param string cls 全选/反选按钮class
 * @param string name 被选择的控件组名字
 */
function iCheckStyle(cls, name){
	// 样式
	$('input').iCheck({
		checkboxClass: 'icheckbox_flat-blue',
		radioClass: 'iradio_flat-blue'
	});
	// 事件
	if (cls !='' && name!='') {
		var selector = $("." + cls);
		var eles = $("[name='"+name+"']");
		
		$(selector).on('ifChecked', function(event){
		  	$(eles).iCheck('check');
		});
		$(selector).on('ifUnchecked', function(event){
		  	$(eles).iCheck('uncheck');
		});
	}
}


/**
 * 检查控件是否被选中（chebox多选控件）
 * @param string name 被选择的控件组名字
 * @returns bool 
 */
function check_select(name){
	var eles = $("[name='"+name+"']");
	if($(eles).is(':checked')){
		return true;
	}
	return false;
}

/**
 * 获取checkbox组的值数组
 * @param string name
 * @returns array
 */
function get_checkbox_group_value(name){
	var eles = $("[name='"+name+"']");
	var values = [];
	$.each(eles, function(k,v){
		if($(v).is(':checked')){
			values.push($(v).val());
		}
	});
	return values;
}

/**
 * 获取select组的值数组
 * @param string name
 * @returns array
 */
function get_select_group_value(name){
	var eles = $("[name='"+name+"']");
	var values = [];
	$.each(eles, function(k,v){
		if($(v).is(':selected')){
			values.push($(v).val());
		}
	});
	return values;
}

/**
 * @desc 刷新页面
 * @param time : time毫秒之后刷新
 */
function pageReload(time) {
	time = time === '' ? 500 : time;
	setTimeout(function(){
		location.reload();
	}, time);
}

/**
 * @desc 关闭页面
 * @param time : time毫秒之后关闭
 */
function closeWindow(time) {
	time = time === '' ? 500 : time;
	setTimeout(function(){
		window.close();
	}, time);
}

/**
 * @desc detapicker 日期选择插件样式
 * @param form 表单id
 * @param cless form表单中需要设置样式的input的cless选择器
 */
function datepickerStyle(form,cls) {
	$(form).on('focus', cls, function(){
		$(this).datepicker({
			language: 'zh-CN',
			weekStart: 1,
			todayBtn: 'linked',
			autoclose: 1,
			todayHighlight:1, 
			format: 'yyyy-mm-dd'
		});
	});

};

/**
 * AJAX方式表单提交()
 */
function ajaxSubmitFrom(){
	$('.submit').click(function () {
		var self = $(this),
		form = self.parents("form");//找到祖先元素里面的form元素

		url        = form.attr('action');
		formData   = form.serialize();

	    $.post(url,formData,function(json) {
      		if (json.code) {
      			layer.alert(json.msg,{icon: 1});
      			pageReload(2000);
      		} else {
      			layer.alert(json.msg,{icon: 5});//图标5是为了区分js的报错
      		}

      	},"json");
	});
}

/**
 * @desc 关闭iframe层
 * @param time : time毫秒之后刷新
 */
function closeIframe(time) {
	time = time === '' ? 500 : time;
	setTimeout(function(){
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
      	parent.layer.close(index); //再执行关闭   
	}, time);
}

/**
 * @desc 跳转
 * @param url
 * @param time
 */
function goLocation(url, time) {
	url = url || location.href;
	time = time === '' ? 500 : time;
	setTimeout(function(){
		location.href = url;
	}, time);
}


/**
 * @desc  字符串是否存在数组中
 * @param stringToSearch
 * @param arrayToSearch
 */
function in_array(stringToSearch, arrayToSearch) {
	for (s = 0; s < arrayToSearch.length; s++) {
		thisEntry = arrayToSearch[s].toString();
		if (thisEntry == stringToSearch) {
			return true;
		}
	}
	return false;
}

/**
 * @desc  返回数组中指定的元素索引
 * @param string
 * @param array
 */
function get_index(string, array) {
	for (var i = 0; i < array.length; i++) {
		if (array[i] == string) {
			return i;
		}
	}
	return false;
}

/**
 * @desc  删除数组中指定元素
 * @param string
 * @param array
 */
function splice_del(string, array) {
	var index = get_index(string, array);

	if (index) {
		array.splice(index,1);
	}
}


/**
 * 检查浏览器是否支持某属性
 * @param attrName
 * @param attrValue
 * @returns {boolean}
 */
function attr_support(attrName, attrValue) {
	try {
		var element = document.createElement('div');
		if (attrName in element.style) {
			element.style[attrName] = attrValue;
			return element.style[attrName] === attrValue;
		} else {
			return false;
		}
	} catch (e) {
		return false;
	}
}


