<?php

namespace constant;

/**
 * 发货状态
 *
 */
class ShippingStatus {

	const NOT_SHIPPED = 1;
	const SHIPPED     = 2;

	public static $en			= array(
		self::NOT_SHIPPED => 'Not shipped',
		self::SHIPPED     => 'Shipped'
	);

	public static $cn			= array(
		self::NOT_SHIPPED => '未发货',
		self::SHIPPED     => '已发货'
	);
}
