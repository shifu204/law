<?php

namespace constant;

/**
 * 支付方式
 *
 */
class PayName {

	const COD_PAY    = 1;
	const WX_PAY     = 2;
	const ALI_PAY    = 3;
	const ONLINE_PAY = 4;


	public static $en			= array(
		self::COD_PAY    => 'COD pay',
		self::WX_PAY     => 'WeChat pay',
		self::ALI_PAY    => 'Alipay pay',	
		self::ONLINE_PAY => 'Online pay'
	);

	public static $cn			= array(
		self::COD_PAY    => '货到付款',
		self::WX_PAY     => '微信支付',
		self::ALI_PAY    => '支付宝支付',
		self::ONLINE_PAY => '银联支付'
	);
}
