<?php

namespace constant;

/**
 * 订单状态
 *
 */
class OrderStatus {

	const TO_BE_CONFIRMED = 1;
	const CONFIRMED       = 2;
	const RECEIVED_GOODS  = 3;
	const CANCELED        = 4;
	const COMPLETED       = 5;
	const OBSOLETE        = 6;

	public static $en			= array(
		self::TO_BE_CONFIRMED => 'To be confirmed',
		self::CONFIRMED       => 'Confirmed',
		self::RECEIVED_GOODS  => 'Received goods',	
		self::CANCELED        => 'Canceled',
		self::COMPLETED       => 'Completed',
		self::OBSOLETE        => 'Obsolete'
	);

	public static $cn			= array(
		self::TO_BE_CONFIRMED => '待确认',
		self::CONFIRMED       => '已确认',
		self::RECEIVED_GOODS  => '已收货',
		self::CANCELED        => '已取消',
		self::COMPLETED       => '已完成',
		self::OBSOLETE        => '已作废'
	);
}
