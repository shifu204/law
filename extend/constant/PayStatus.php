<?php

namespace constant;

/**
 * 支付状态
 *
 */
class PayStatus {

	const UNPAID      = 1;
	const HAS_TO_PAID = 2;

	public static $en			= array(
		self::UNPAID      => 'Unpaid',
		self::HAS_TO_PAID => 'Has to paid'
	);

	public static $cn			= array(
		self::UNPAID      => '未支付',
		self::HAS_TO_PAID => '已支付'
	);
}
