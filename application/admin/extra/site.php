<?php


return [
    // 网站信息
    'name'        => 'smart',
    'title'       => 'smart.platform',
    'version'     => 'v1.0',
    'keywords'    => '大数据',
    'description' => '天波数据管理系统',
    'copyright'   => 'Copyright &copy; 2017',
    'seo_url'     => 'https://www.syhuo.net',
    'status'      => '开发ing...',

];