<?php
//------------------------
// 公共函数
//-------------------------

// use think\Session;
// use think\Response;
// use think\Request;
// use think\Url;
// use think\Db;
// use classes\Auth;
// use debug\Slog;

// /**
//  * 二维数组转换为一维数组
//  * @author 李嘉华
//  * @param  [type] $array 二维数组
//  * @param  string $a     转换的字段
//  * @return [type]        一维数组
//  */
// function two_to_one($array, $a = '') {
// 	$_array = array();
// 	for ($i=0; $i < count($array); $i++) {
// 		$b = $array[$i][$a];
// 		array_push($_array,$b);
// 	}
// 	return $_array;
// }

// /**
//  * 根据二维数组中的attr_id排序
//  * @author 李嘉华
//  */
// function attr_id_sort($a,$b) {
//     if ($a['attr_id'] == $b['attr_id']) {
//         return 0;
//     }
//     return ($a['attr_id'] < $b['attr_id']) ? -1 : 1 ;
// }

// /**
//  * 将换行符转换为逗号
//  * @author 李嘉华
//  */
// function replace_rn($str){
//     $_str = str_replace(array("\r\n", "\r", "\n"), ",", $str); 
//     return $_str;
// }

// /**
//  * 删除文件
//  * @param string $dir_name
//  * @return bool
//  */
// function delete_file($path,$file){
//     $file_path = show_image($path,$file);

//     if (file_exists(PUBLIC_PATH . $file_path) == false) {
//         // $this->error('文件不存在');
//     } else {
//         @unlink(PUBLIC_PATH . $file_path);
//     }
// }



// /**
//  * 循环删除目录和文件
//  * @param string $dir_name
//  * @return bool
//  */
// function delete_dir_file($dir_name)
// {
//     $result = false;
//     if (is_dir($dir_name)) {
//         if ($handle = opendir($dir_name)) {
//             while (false !== ($item = readdir($handle))) {
//                 if ($item != '.' && $item != '..') {
//                     if (is_dir($dir_name . DS . $item)) {
//                         delete_dir_file($dir_name . DS . $item);
//                     } else {
//                         unlink($dir_name . DS . $item);
//                     }
//                 }
//             }
//             closedir($handle);
//             if (rmdir($dir_name)) {
//                 $result = true;
//             }
//         }
//     }

//     return $result;
// }


// /**
//  * 显示图片
//  * @param  $path 图片所属路径文件夹
//  * @param  $image 图片名字
//  * @return string 图片完整路径（不存图片路径默认返回默认图片）
//  */
// function show_image($path,$image){
// 	if (!$image) {
// 		return DS . 'uploads' . DS . 'default.jpg';
// 	} else {
// 		return DS . 'uploads' . DS . $path . DS . $image;
// 	}

// }

// /**
//  * 生产时间戳加随机数字的图片名字
//  */
// function get_image_name(){
//     $name = time() . rand(1000, 9999) . '.jpg';
//     return $name;
// }

// /**
//  * 生成日期文件夹
//  */
// function make_date_dir(){
//         return date('Y/m/d');
// }

// /**
//  * 时间戳格式化
//  *
//  * @param int $time         
//  * @return string 完整的时间显示
//  * @author huajie <banhuajie@163.com>
//  */
// function time_format($time = NULL, $format = 'Y-m-d H:i') {
//     if (empty ( $time ))
//         return '';
    
//     $time = $time === NULL ? time() : intval ( $time );
//     return date ( $format, $time );
// }
// function day_format($time = NULL) {
//     return time_format ( $time, 'Y-m-d' );
// }
// function hour_format($time = NULL) {
//     return time_format ( $time, 'H:i' );
// }


// /**
//  * ajax返回函数
//  * @param  [type] $data   [description]
//  * @param  [type] $info   [description]
//  * @param  [type] $status [description]
//  * @return [type]         [description]
//  */
// function return_format($data = array(), $info = '', $status = 0) {
//     return array('data' => $data, 'info' => $info, 'status' => $status);
// }

// /**
//  * socketlog日志调试
//  * @param string $log 日志消息
//  * @param string $type 日志类型
//  * @param string $css 日志样式
//  * @return mixed
//  * @throws Exception
//  */
// function slog($log,$type='log',$css='')
// {

//     if(is_string($type))
//     {
//         $type=preg_replace_callback('/_([a-zA-Z])/',create_function('$matches', 'return strtoupper($matches[1]);'),$type);
//         if(method_exists('\debug\Slog',$type) || in_array($type,Slog::$log_types))
//         {
//            return  call_user_func(array('\debug\Slog',$type),$log,$css);
//         }
//     }

//     if(is_object($type) && 'mysqli'==get_class($type))
//     {
//            return Slog::mysqlilog($log,$type);
//     }

//     if(is_resource($type) && ('mysql link'==get_resource_type($type) || 'mysql link persistent'==get_resource_type($type)))
//     {
//            return Slog::mysqllog($log,$type);
//     }


//     if(is_object($type) && 'PDO'==get_class($type))
//     {
//            return Slog::pdolog($log,$type);
//     }

//     throw new Exception($type.' is not SocketLog method');
// }

// // 用户密码加密方法
// function user_encryption($password){
//     return md5(md5($password).config('Auth.user_auth_key'));
// }

// // 检查密码
// function check_password($password,$uid){
//     $_password = user_encryption($password);

//     if ($_password == model('AdminUser')->get($uid)->password) {
//         return true;
//     } else {
//         return false;
//     }

// }

// /**
//  * 元素（按钮）根据规则显示
//  * @param string $auth_name 规则名称
//  * @return bool
//  */
// function show_auth($auth_name){
//     $auth     = new Auth();
//     $admin_id = Session::get('admin_id');
//     // halt($auth);

//     if ($auth->check($auth_name, $admin_id) || $admin_id == 1) {
//         return true;
//     } else {
//         return false;
//     }
// }

// /**
//  * 菜单折叠展开判断
//  * @param string $controller 
//  * @param string $action 
//  * @return bool
//  */
// function class_active($controller,$action = ""){
//     if ($action != "") {
//         if ($controller == Request::instance()->controller() AND $action == Request::instance()->action()) {
//             return true;
//         } else {
//             return false;
//         }
//     }

//     $controller = explode('/',$controller); 

//     foreach ($controller as $k => $v) {
//         if ($v == Request::instance()->controller()) {
//             return true;
//         }
//     }
//     return false;
// }


// /**
//  * 发送邮件
//  * @param string $to 
//  * @param string $title 
//  * @param string $message 
//  * @return bool
//  */
// function send_mail($to, $title, $message) {
//     $smtpInfo = model('SystemConfig')->where('name','smtp_parameter')->find();
//     $smtpInfo = json_decode($smtpInfo['value'],true);
// // halt($smtpInfo);
//     $mail = new PHPMailer();

//     $mail->CharSet    = 'UTF-8';
//     $mail->IsSMTP();                                // 告诉类使用SMTP
//     $mail->SMTPAuth   = true;                       // 启用SMTP认证
//     $mail->Port       = $smtpInfo['smtp_port'];     // 设置SMTP服务器端口
//     $mail->Host       = $smtpInfo['smtp_server'];   // SMTP 服务器
//     $mail->Username   = $smtpInfo['smtp_user'];     // SMTP 服务器 用户邮箱
//     $mail->Password   = $smtpInfo['smtp_password']; // SMTP 服务器 用户密码
//     $mail->SMTPSecure = $smtpInfo['smtp_secure'];   // 加密方式
//     $mail->From       = $smtpInfo['smtp_user'];
//     $mail->FromName   = 'shifu204';
//     $mail->AddAddress($to);
//     $mail->Subject  = $title;
//     $mail->MsgHTML($message);
//     $mail->IsHTML(true);                            // send as HTML

//     $res = $mail->Send();

//     if ($res) {
//         $return = array('status' => 1, 'info' => '邮件发送成功');
//     } else {
//         $return = array('status' => 0, 'info' => $mail->ErrorInfo);
//     }

//     return $return;
// }




