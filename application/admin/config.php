<?php
use \think\Request;

return [
    // +----------------------------------------------------------------------
    // | 后台模板设置
    // +----------------------------------------------------------------------

	// 模板路径
    'template' => [
        'view_path' => '../public/view/admin/'
    ],

    // 模板参数替换
	'view_replace_str' => [
	    '__ROOT__'           => '/',
	    '__PUBLIC__'         => '/static',
        '__AJS__'            => '/static/admin/js',
        '__ACSS__'           => '/static/admin/css',
        '__REQUIRE__'        => '/static/require',
        '__DOWNLOAD__'        => '/static/template',
	],

    // +----------------------------------------------------------------------
    // | auth 设置
    // +----------------------------------------------------------------------

    // auth配置
    'auth'    => [
        // 权限开关
        'auth_on'           => 1,
        // 认证方式，1为实时认证；2为登录认证。
        'auth_type'         => 1,
        // 用户组数据不带前缀表名
        'auth_group'        => 'auth_group',
        // 用户-用户组关系不带前缀表
        'auth_group_access' => 'auth_group_access',
        // 权限规则不带前缀表
        'auth_rule'         => 'auth_rule',
        // 用户信息不带前缀表
        'auth_user'         => 'admin_user',
    ],
];