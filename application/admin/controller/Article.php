<?php
//------------------------
// 资讯控制器
//-------------------------
namespace app\admin\controller;
use app\common\controller\AdminBase;

class Article extends AdminBase{
	public function index(){
		$cate_id=input('post.cate_id',0,'intval');
        $is_on=input('post.is_on',1,'intval');
        $is_examine=input('post.is_examine',0,'intval');
        $start=input('post.start','','trim');
        $stop=input('post.stop','','trim');
        $new_title=input('post.new_title','','trim');
        $author=input('post.author','','trim');
        $source=input('post.source','','trim');
        $sort_num=input('post.sort_num',0,'intval');

        $where=array();
        if($cate_id){
        	$where['cate_id']=array('eq',$cate_id);
        }
        if($is_on){
        	$_is_on=$is_on==2?0:1;
        	$where['is_on']=array('eq',$_is_on);
        }
        if($is_examine){
        	$_is_examine=$is_examine==2?0:1;
        	$where['is_examine']=array('eq',$_is_examine);
        }
        if ($start) {
            $_start = strtotime(date('Y-m-d 00:00:00', strtotime($start)));
            $where['addtime'] = array('egt',$_start);
        }
        if ($stop) {
            $_stop  = strtotime(date('Y-m-d 23:59:59', strtotime($stop)));
            $where['addtime'] = array('elt',$_stop);
        }
        if($new_title){
        	$where['new_title']=array('like','%'.$new_title.'%');
        }
         if($author){
        	$where['author']=array('like','%'.$author.'%');
        }
         if($source){
        	$where['source']=array('like',$source);
        }
        $cateinfo=model('ArticleCate')->getTree();
        $this->assign('cateinfo',$cateinfo);
        

        $info=model('Article')->where($where)->order($sort_num)->
        paginate(10,false,['query' => [
            
            'cate_id'     =>$cate_id,
            'is_on'      =>$is_on,
            'is_examine'  =>$is_examine,
            'start'       =>$start,
            'stop'        =>$stop,
            'new_title'   =>$new_title,
            'author'      =>$author,
            'source'      =>$source,
            'sort_num'        =>$sort_num,
            ] ]);
// halt($cateinfo);
        $this->assign('cate_id',$cate_id);
        $this->assign('is_on',$is_on);
        $this->assign('is_examine',$is_examine);
        $this->assign('start',$start);
        $this->assign('stop',$stop);
        $this->assign('new_title',$new_title);
        $this->assign('author',$author);
        $this->assign('source',$source);
        $this->assign('sort_num',$sort_num);
		$this->assign('info',$info);
		return $this->fetch('index');
	}

	public function add(){
		$cateinfo=model('ArticleCate')->getTree();
		$this->assign('cateinfo',$cateinfo);
		return $this->fetch('add');
	}

	public function addProc(){
		
			$data=input('');
			$data['addtime']=time();
			
			$validate_res=$this->validate($data,'Article');
			if($validate_res!==true){
				$this->error($validate_res);
			}
				$res=model('Article')->allowField(true)->save($data);
				if($res!==false){
					$this->success('添加成功');
				}else{
					$this->error(model('Article')->getError());
				}

			
		
	}

	public function edit(){
		$id=input('get.id',0,'intval');
		$cateinfo=model('ArticleCate')->getTree();
		
		$info=model('Article')->get($id);
		$this->assign('cateinfo',$cateinfo);
		
		$this->assign('info',$info);
		return $this->fetch('edit');
	}

	public function editProc(){
		
			
			$data=input('');
			$validate_res=$this->validate($data,'Article');
			if($validate_res!==true){
				$this->error($validate_res);
			}
				$res=model('Article')->allowField(true)->save($data,$data['id']);
				if($res!==false){
					$this->success('修改成功');
				}else{
					$this->error(model('ArticleCate')->getError());
				}
			
		

	}

	public function uploadLogo(){
		$id=input('id',0,'intval');
		$path=input('path','other','trim');

		if (!$id) {
            $this->error('缺少要上传图片的品牌id');
        }

         // 获取表单上传文件 
        $file = request()->file('logo');

        $info = $file->validate(config('image_validate'))->move(config('image_path') . $path);

         // 删除替换前的图片
        $image = db('Article')->where('id',$id)->field('logo')->find();

        // 删除文件
        delete_file('article',$image['logo']);

         if($info){
            // 返回图片地址
            $return_url = show_image($path, $info->getSaveName());

            // 保存图名字到数据库
            $data['logo'] = $info->getSaveName();
            $data['id'] = $id;

            $res =  model('Article')->allowField(true)->save($data,$data['id']);

            $return = array('msg' => '上传成功','url'=>$return_url, 'code' => 1);
        }else{
            // 上传失败获取错误信息
            $return = array('msg' => $file->getError(), 'code' => 0);
        }
        echo json_encode($return);



	}

	public function delete(){
		$id=input('post.id',0,'intval');
		if(!$id){
           $this->error('请选择要删除的数据');
		}
		 $image = db('Article')->where('id',$id)->field('logo')->find();

        // 删除文件
        delete_file('article',$image['logo']);

		$res=model('Article')->where('id',$id)->delete();
		if($res!==false){
			$this->success('删除成功');
		}else{
			$this->error(model('Article')->getError());
		}
	}

	 // 批量删除>>调用
    private function _batchDelete($id){
        $where['id'] = array('eq',$id);

        //  先找出图片路径
        $image = db('Article')->where('id',$id)->field('logo')->find();

        $res = model('Article')->where($where)->delete();

        if ($res == false) {
           return false;
        } else {
            // 删除文件
            delete_file('article',$image['logo']);

            return true;
        }

    }

     // 批量处理
    public function batchProc(){
        $data = $this->request->param();

        $ids = implode(',',$data['ids']);
        $map['id']  = ['in',$ids];
        $type = $data['type'];

        switch ($type) {
            case 'delete':
                $successData = array();
                $failData = array();
                for ($i=0; $i < count($data['ids']); $i++) { 
                    $result = $this->_batchDelete($data['ids'][$i]);
                    if ($result) {
                        array_push($successData, $data['ids'][$i]);
                    } else {
                        array_push($failData, $data['ids'][$i]);
                    }
                }
                $success = implode(',',$successData);
                $fail   = implode(',',$failData);
                $this->success('删除成功id为：'.$success.'，删除失败id为：'.$fail);
                break;
            default:
                $this->error('非法操作');
                break;
        }

        if ($res === false) {
            $this->error($this->GoodsBrandModel->getError());
        } else {
            $this->success('批量'.$act.'成功');
        }

    }


}