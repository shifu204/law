<?php
//------------------------
// 资讯控制器
//-------------------------
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\common\model\Deadbeat as DeadbeatModel;

class Deadbeat extends AdminBase{

    protected $DeadbeatModel;

    protected function _initialize()
    {
        parent::_initialize();
        $this->DeadbeatModel       = new DeadbeatModel();
    }

	public function index(){
        $name       = $this->request->param('name','','trim');
        $card_sn    = $this->request->param('card_sn','','trim');
   
        $where = array();

        if ($name) {
             $where['name'] = array('like','%'.$name.'%');
        }

        if ($card_sn) {
             $where['card_sn'] = array('like','%'.$card_sn.'%');
        }

        $info = model('Deadbeat')->where($where)->paginate(30,false,[
            'name'    => $name,
            'card_sn' => $card_sn,
        ]);

        $this->assign('name',$name);
        $this->assign('card_sn',$card_sn);
		$this->assign('info',$info);

		return $this->fetch('index');
	}



	public function edit(){
		$id = input('get.id',0,'intval');

		$info = model('Deadbeat')->get($id);
		// halt($info);die;
		$this->assign('info',$info);

		return $this->fetch('edit');
	}

	public function editProc(){
		$data = input('');

		$validate_res=$this->validate($data,'Deadbeat');
		if ($validate_res !== true) {
			$this->error($validate_res);
		}

		$res = model('Deadbeat')->allowField(true)->save($data,$data['id']);

		if($res!==false){
			$this->success('修改成功');
		}else{
			$this->error(model('Deadbeat')->getError());
		}

	}



    // 用户详情
    public function detail(){
        $id = $this->request->param('id',0,'intval');

        $data = $this->DeadbeatModel->get($id);
// halt($data);
        $this->assign('data',$data);
        return $this->fetch('detail');
    }



	public function delete(){
		$id = input('post.id',0,'intval');
		if(!$id){
           $this->error('请选择要删除的数据');
		}

		$res = model('Deadbeat')->where('id',$id)->delete();
		if($res!==false) {
			$this->success('删除成功');
		} else {
			$this->error(model('Deadbeat')->getError());
		}
	}

	 // 批量删除>>调用
    private function _batchDelete($id){
        $where['id'] = array('eq',$id);

        $res = model('Deadbeat')->where($where)->delete();

        return true;
    }

     // 批量处理
    public function batchProc(){
        $data = $this->request->param();

        $ids = implode(',',$data['ids']);
        $map['id']  = ['in',$ids];
        $type = $data['type'];

        switch ($type) {
            case 'delete':
                $successData = array();
                $failData = array();
                for ($i=0; $i < count($data['ids']); $i++) { 
                    $result = $this->_batchDelete($data['ids'][$i]);
                    if ($result) {
                        array_push($successData, $data['ids'][$i]);
                    } else {
                        array_push($failData, $data['ids'][$i]);
                    }
                }
                $success = implode(',',$successData);
                $fail   = implode(',',$failData);
                $this->success('删除成功id为：'.$success.'，删除失败id为：'.$fail);
                break;
            case 'output':
                // $this->error('此为付费功能，请先充值vip');
                $this->allOut2($data['ids']);

            default:
                $this->error('非法操作');
                break;
        }

        if ($res === false) {
            $this->error($this->GoodsBrandModel->getError());
        } else {
            $this->success('批量'.$act.'成功');
        }

    }

    /**
     * @desc 导入设备页面
     */
    public function importView() {
        return $this->fetch('import_device');
    }

    /**
     * @desc 导入设备操作
     */
    public function importProc() {
        $act = input('act','','trim');

        action('common/Excel/import',array('act' => $act));

    }


    /**
     * @desc 导出到word操作
     */
    public function out($id = 0) {
        $deadbeat_id = 0;
        if ($id != 0) {
            $deadbeat_id = $id;
        } else {
            $deadbeat_id = input('id','');
        }
        
        $res = model('Deadbeat')->where('id',$deadbeat_id)->find();

        if ($res) {
            action('common/Word/outPut',array('act' => 'deadbeat', 'info' => ['deadbeat_id' => $deadbeat_id]));
        } else {
            $this->error('没有对应导出数据');
        }

    }


}