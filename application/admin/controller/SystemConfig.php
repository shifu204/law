<?php

//------------------------
// 系统配置控制器
//-------------------------
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\SystemConfig as SystemConfigModel;
use app\home\controller\Wxpay;
use think\Config;
use think\Db;
use constant\PayStatus;

class SystemConfig extends AdminBase
{
    protected $SystemConfigModel;
    protected $WxpayController;

    protected function _initialize()
    {
        parent::_initialize();
        $this->SystemConfigModel       = new SystemConfigModel();
        $this->WxpayController         = new Wxpay();

    }

    // 设置微信接口参数
    public function wechatApi(){
        if ($this->request->isGet()) {
            // 获取微信api相关参数
            $info = $this->SystemConfigModel->where('name','wechat_api_parameter')->find();
            $_info = json_decode($info['value'],true);

            $this->assign('id',$info['id']);
            $this->assign('info',$_info);
            return view('wechat_api');
        }

        $data = $this->request->post();

        $validate_result = $this->validate($data, 'SystemConfig.wx_api');

        if ($validate_result !== true) {
            $this->error($validate_result);
        }

        $_data = json_encode($data);

        $res = $this->SystemConfigModel->update(['id' => $data['id'], 'value' => $_data]);

        if ($res === false) {
            $this->error($this->SystemConfigModel->getError());
        } else {
            $this->success('数据修改成功');
        }

    }

    // 设置微信支付参数
    public function wechatPay(){
        if ($this->request->isGet()) {
            // 获取微信api相关参数
            $info = $this->SystemConfigModel->where('name','wechat_pay_parameter')->find();
            $_info = json_decode($info['value'],true);

            $this->assign('id',$info['id']);
            $this->assign('info',$_info);
            return view('wechat_pay');
        }

        $data = $this->request->post();

        $validate_result = $this->validate($data, 'SystemConfig.wx_pay');

        if ($validate_result !== true) {
            $this->error($validate_result);
        }

        $_data = json_encode($data);

        $res = $this->SystemConfigModel->update(['id' => $data['id'], 'value' => $_data]);

        if ($res === false) {
            $this->error($this->SystemConfigModel->getError());
        } else {
            $this->success('数据修改成功');
        }
    }

    // 支付测试
    public function payTest(){

        $order_sn     = createOutTradeNo();
        $actual_price = '0.01';

        $data = array(
            'order_sn'    => $order_sn,
            'payment_fee' => $actual_price
        );

        $qrcode = $this->WxpayController->getQrcode($data);

        if (!$qrcode) {
            $this->error('参数有误，操作失败！');
        } else {
            // 添加一条测试订单数据
            $orderData = array(
                    'order_sn'     => $order_sn,
                    'actual_price' => $actual_price,
                    'user_remark'  => '支付测试',
                    'pay_name'     => 'wx_pay',
                    'add_time'     => time()
                );
            model('Order')->save($orderData);

            // 返回支付二维码
            $qrcode = $this->WxpayController->getQrcode($data);

            $qrcode_url = '<img src="'. $qrcode. '"  class="img-rounded">';

            $data = array(
                    'qrcode_url' => $qrcode_url,
                    'order_sn' => $order_sn
                );

            $this->success($data);
        }


    }

    /**
     * @desc 验证微信支付完成与否
     */
    public function checkWxpayFinish() {
        $order_sn = $this->request->param('order_sn', '', 'trim');
        if (!$order_sn) {
            $this->error('缺少订单号');
        }

        $status = Db('Order')->where('order_sn',$order_sn)->field('pay_status')->find();

        if ($status['pay_status'] == PayStatus::HAS_TO_PAID) {
            $this->success('支付成功');
        } else {
            $this->error('正在支付');
        }
    }

    // 设置smtp邮箱参数
    public function smtpSet(){
        if ($this->request->isGet()) {

            // 获取微信api相关参数
            $info = $this->SystemConfigModel->where('name','smtp_parameter')->find();
            $_info = json_decode($info['value'],true);

            $this->assign('id',$info['id']);
            $this->assign('info',$_info);
            return view('smtp_set');
        }

        $data = $this->request->post();

        $validate_result = $this->validate($data, 'SystemConfig.smtp');

        if ($validate_result !== true) {
            $this->error($validate_result);
        }

        $_data = json_encode($data);

        $res = $this->SystemConfigModel->update(['id' => $data['id'], 'value' => $_data]);

        if ($res === false) {
            $this->error($this->SystemConfigModel->getError());
        } else {
            $this->success('数据修改成功');
        }
    }

    // 测试smtp邮箱服务器
    public function smtpTest(){
        $data = $this->request->post();

        $to    = $data['mailbox'];
        $title = '测试邮件';
        $message = 'Hello';

        $res = send_mail($to, $title, $message);

        return $res;
    }



}