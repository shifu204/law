<?php
//------------------------
// 资讯分类控制器
//-------------------------
namespace app\admin\controller;
use app\common\controller\AdminBase;

class ArticleCate extends AdminBase{
	public function index(){
		$info=model('ArticleCate')->getTree();
		$this->assign('info',$info);
		return $this->fetch('index');
	}

	public function add(){
		$cateinfo=model('ArticleCate')->getTree();
		$this->assign('cateinfo',$cateinfo);
		return $this->fetch('add');
	}

	public function addProc(){
		
			$data=input('');
			
			
			$validate_res=$this->validate($data,'ArticleCate');
			if($validate_res!==true){
				$this->error($validate_res);
			}
				$res=model('ArticleCate')->allowField(true)->save($data);
				if($res!==false){
					$this->success('添加成功');
				}else{
					$this->error(model('ArticleCate')->getError());
				}

			
		
	}

	public function edit(){
		$id=input('get.id',0,'intval');
		$cateinfo=model('ArticleCate')->getTree();
		$catechildreninfo=model('ArticleCate')->getChildren($id);
		$info=model('ArticleCate')->where('id',$id)->find();
		$this->assign('cateinfo',$cateinfo);
		$this->assign('catechildreninfo',$catechildreninfo);
		$this->assign('info',$info);
		return $this->fetch('edit');
	}

	public function editProc(){
		
			
			$data=input('');
			$validate_res=$this->validate($data,'ArticleCate');
			if($validate_res!==true){
				$this->error($validate_res);
			}
				$res=model('ArticleCate')->allowField(true)->save($data,$data['id']);
				if($res!==false){
					$this->success('修改成功');
				}else{
					$this->error(model('ArticleCate')->getError());
				}
			
		

	}

	public function delete(){
		$id=input('post.id',0,'intval');
		if(!$id){
           $this->error('请选择要删除的数据');
		}
		$has=model('ArticleCate')->where('parent_id',$id)->find();
		if($has){
			$this->error('当前分类下还有子分类');

		}
		$res=model('ArticleCate')->where('id',$id)->delete();
		if($res!==false){
			$this->success('删除成功');
		}else{
			$this->error(model('ArticleCate')->getError());
		}
	}


}