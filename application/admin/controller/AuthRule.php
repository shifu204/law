<?php

//------------------------
// 权限管理控制器
//-------------------------
namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\AuthRule as AuthRuleModel;
use think\Db;

class AuthRule extends AdminBase
{
    protected $AuthRuleModel;

    protected function _initialize()
    {
        parent::_initialize();
        $this->AuthRuleModel        = new AuthRuleModel();
    }



    // 认证规则列表
    public function index(){
    	$data = $this->AuthRuleModel->getTree();

        // halt($data);

        $this->assign('data',$data);

        return $this->fetch('index');
    }


    // 添加页面
    public function add(){
    	$pid = $this->request->param('pid',0,'intval');

        $this->assign('pid',$pid);
    	return $this->fetch('add');
    }

    // 编辑页面
    public function edit(){
        $id = $this->request->param('id',0,'intval');
        $data = $this->AuthRuleModel->where("id",$id)->find();

        $this->assign('data',$data);
        return $this->fetch('edit');
    }

    /**
     * @desc 添加、编辑操作
     */
    public function addProc() {
        $data = $this->request->param();
        $id   = $this->request->param('id',0,'intval');

        $validate_result = $this->validate($data, 'AuthRule');
        if ($validate_result !== true) {
            $this->error($validate_result);
        }

        $data['type'] = 1;

        if ($id) {
            // 编辑
            $res = $this->AuthRuleModel->allowField(true)->save($data,$id);
            $act = "编辑";
        } else {
            // 添加
            $res = $this->AuthRuleModel->allowField(true)->save($data);
            $act = "添加";
        }

        if ($res === false) {
            $this->error($this->AuthRuleModel->getError());
        } else {
            $this->success('规则'.$act.'成功');
        }
    }



    // 删除操作
    public function delete(){
        $id = $this->request->param('id',0,'intval');

        if (!$id) {
            $this->error('请选择需要删除的数据');
        }

        $has = $this->AuthRuleModel->where('pid',$id)->find();

        if ($has) {
            $this->error('当前规则下面还有子规则');
        }

        $res = $this->AuthRuleModel->where('id',$id)->delete();

        if ($res === false) {
            $this->error($this->AuthRuleModel->getError());
        } else{
            $this->success('删除成功');
        }
    }



}