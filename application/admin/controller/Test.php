<?php

//------------------------
// 测试控制器
//-------------------------
namespace app\admin\controller;

use app\common\controller\AdminBase;

use app\common\controller\Wxpay;

use think\Config;
use think\Db;

class Test extends AdminBase
{

    protected $WxpayController;

    protected function _initialize()
    {
        parent::_initialize();


        require_once ROOT_PATH  . 'extend/classes/Wxpay/example/phpqrcode/phpqrcode.php';
        $this->WxpayController = new Wxpay();

    }


    public function index(){

        $data = array(
                'order_sn' => 'sf123',
                'payment_fee' => '1'
            );


        $qrcode = $this->WxpayController->getQrcode($data);

        halt($qrcode);


    }

    public function qrocde(){
        error_reporting(E_ERROR);
        $data = $this->request->param();
        halt($data);
        $url = urldecode($_GET["data"]);
        QRcode::png($url, false, QR_ECLEVEL_L, 15, 0);
    }





}