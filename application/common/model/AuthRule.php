<?php
//------------------------
// 权限管理模型
//-------------------------
namespace app\common\model;

use think\Model;

class AuthRule extends Model{

	// 获取分类树
	public function getTree(){
		$data = $this->select();
		return $this->_reSort($data);
	}

	// 无限级向下递归制作分类树
	private function _reSort($data, $parent_id=0, $level=0, $isClear=TRUE){
		static $ret = array();
		if($isClear)
			$ret = array();
		foreach ($data as $k => $v){
			if($v['pid'] == $parent_id){
				$v['level'] = $level;
				$ret[] = $v;
				$this->_reSort($data, $v['id'], $level+1, FALSE);
			}
		}
		return $ret;
	}



	// 获取子分类
	public function getChildren($id){
		$data = $this->select();
		return $this->_children($data, $id);
	}

	// 无限级向下递归获取子分类
	private function _children($data, $parent_id=0, $isClear=TRUE){
		static $ret = array();
		if($isClear)
			$ret = array();
		foreach ($data as $k => $v){
			if($v['pid'] == $parent_id){
				$ret[] = $v['id'];
				$this->_children($data, $v['id'], FALSE);
			}
		}
		return $ret;
	}
}