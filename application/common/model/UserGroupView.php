<?php
//------------------------
// 用户、用户组视图
// platform_admin_user , platform_auth_group , platform_auth_group_access
//-------------------------
namespace app\common\model;

use think\Model;
use think\Db;

class UserGroupView extends Model{

	// 设置platform_auth_group_access为主表
	protected $table = 'platform_auth_group_access';

	//自定义初始化
    protected function initialize()
    {
        parent::initialize();
        //TODO:自定义的初始化,连表
        $this->join('platform_auth_group','platform_auth_group_access.group_id = platform_auth_group.id','LEFT')->field('platform_auth_group.title AS group_name');

        $this->join('platform_admin_user','platform_auth_group_access.uid = platform_admin_user.id','LEFT')->field('platform_admin_user.username AS user_name');
    }

}