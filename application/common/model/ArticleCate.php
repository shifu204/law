<?php
//------------------------
// 资讯分类模型
//-------------------------
namespace app\common\model;
use think\Model;

class ArticleCate extends Model{
	//分类树
	public function getTree(){
		$data = $this->select()->toArray();
		return $this->_reSort($data);
	}

	//无限级分类
	private function _reSort($data, $parent_id=0, $level=0, $isClear=TRUE){
		static $ret = array();
		if($isClear)
			$ret = array();
		foreach ($data as $k => $v){
			if($v['parent_id'] == $parent_id){
				$v['level'] = $level;
				$ret[] = $v;
				$this->_reSort($data, $v['id'], $level+1, FALSE);
			}
		}
		return $ret;
	}

	// 获取子分类
	public function getChildren($id){
		$data = $this->select()->toArray();
		return $this->_children($data, $id);
	}

	// 无限级向下递归获取子分类
	private function _children($data, $parent_id=0, $isClear=TRUE){
		static $ret = array();
		if($isClear)
			$ret = array();
		foreach ($data as $k => $v){
			if($v['parent_id'] == $parent_id){
				$ret[] = $v['id'];
				$this->_children($data, $v['id'], FALSE);
			}
		}
		return $ret;
	}
}