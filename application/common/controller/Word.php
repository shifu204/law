<?php
namespace app\common\controller;

use think\Controller;
use think\Request;
use think\Session;
use think\Response;

use app\common\model\Deadbeat as DeadbeatModel;

/**
 * Word导出
 */
class Word extends Controller {


	// 导出
	public function outPut($act,$info = []) {

		set_time_limit(0);

		if (!$act){
		 	$this->error('非法操作');
		}

		switch ($act) {
			// 分拆费用
			case 'deadbeat':
				$return = $this->_outDeadbeat($info);
				break;
			default:
				break;
		}
	}

	// 导出老赖信息
	private function _outDeadbeat($info) {
		$deadbeat_id = $info['deadbeat_id'];
		$res = model('Deadbeat')->where('id',$deadbeat_id)->find()->toArray();

		// 数据处理
		if ($res['date_of_entry'] == '') {
			$res['date_of_entry'] = date("Y-m-d",time());
		}

		$date_of_entry  = getDateFormat($res['date_of_entry']);

		$date_of_open  = getDateFormat($res['date_of_open']);

		$birthday  = getDateFormat($res['birthday']);

		$phpWord = new \PhpOffice\PhpWord\PhpWord();

		//标题样式
		$phpWord->addTitleStyle(1, array('bold' => true, 'size' => 16 ), array('align' => 'center'));

		// 字体样式
		$phpWord->addFontStyle('rStyle', array(  'size' => 14));

		// 段落样式
		$phpWord->addParagraphStyle('pStyle', array('spaceBefore' => 50));


		
		// 添加3号仿宋字体到'FangSong16pt'留着下面使用
		$phpWord->addFontStyle('FangSong14pt', array('name'=>'仿宋', 'size'=>14));
		$phpWord->addFontStyle('FangSong12pt', array('name'=>'仿宋', 'size'=>12));
		// 添加段落样式到'Normal'以备下面使用
		$phpWord->addParagraphStyle(
		  'Normal',array(
		    'align'=>'both',
		    'spaceBefore' => 0,
		    'spaceAfter' => 0,
		    'lineHeight' => 1.19
		  )
		);

		$phpWord->addParagraphStyle(
		  'Small',array(
		    'align'=>'both',
		    'spaceBefore' => 0,
		    'spaceAfter' => 0,
		    'lineHeight' => 0.8
		  )
		);


		$section = $phpWord->createSection();

		/*****************************************************************/

		// 标题
		$section->addTitle(htmlspecialchars('民事起诉状'), 1);

		// $section->addText('文档内容', 'FangSong14pt', 'Normal');

		// $section->addText(''); // 空行

		// 正文
		$textrun = $section->addTextRun();
		$textrun->addText('	原告:', 'FangSong14pt', 'Normal');
		$textrun->addText('交通银行股份有限公司佛山分行，住所地：佛山市禅城区季华五路30号，统一社会信用代码：91440600893543642U。', 'FangSong14pt', 'Normal');

		$section->addText(
			'	法定代表人：徐焱，代表人联系电话：13516525376。',
		 	'FangSong14pt', 'Normal',
		 	null
		);
		// $section->addText(''); // 空行

		$section->addText(
			'	（送达地址：佛山市南海区桂城简平路1号天安创新大厦B座1103室广东禅都律师事务所，联系电话：13432620622,0757-81857075(840))',
		 	'FangSong14pt', 'Normal',
		 	null
		);
		// $section->addText(''); // 空行

		$textrun = $section->addTextRun();
		$textrun->addText('	催收联系人：', 'FangSong14pt', 'Normal');
		$textrun->addText('梁小姐,广东禅都律师事务所律师助理，联系电话：13360321751，固话：0757-81857071(831）。', 'FangSong14pt', 'Normal');

		$textrun = $section->addTextRun();
		$textrun->addText('	委托诉讼代理人：', 'FangSong14pt', 'Normal');
		$textrun->addText('谭启健,广东禅都律师事务所律师，律师执业证：14406201810062918，联系电话：13516525376。', 'FangSong14pt', 'Normal');

		$textrun = $section->addTextRun();
		$textrun->addText('	委托诉讼代理人：', 'FangSong14pt', 'Normal');
		$textrun->addText('王嘉琪,广东禅都律师事务所律师，固话：0757-81857071(840）。', 'FangSong14pt', 'Normal');

		// $section->addText(''); // 空行

		$textrun = $section->addTextRun();
		$textrun->addText('	被告:', 'FangSong14pt', 'Normal');
		$textrun->addText($res['name'].'，'. $res['sex'].'，'.$birthday.'出生，'.$res['nation'].'，住'.$res['permanent_address'].'，身份证号码：'.$res['identity_sn'].'。', 'FangSong14pt', 'Normal');

		$textrun = $section->addTextRun();
		$textrun->addText('	(', 'FangSong14pt', 'Normal');
		$textrun->addText('通讯地址：', 'FangSong14pt', 'Normal');
		$textrun->addText($res['address'] .',联系电话：'. $res['phone'] .',固话：'. $res['telephone'] .'）。', 'FangSong14pt', 'Normal');

		$section->addText(''); // 空行

		$section->addText(
			'	诉讼请求:',
		 	'FangSong14pt', 'Normal',
		 	null
		);

		$str1 = '	1、被告偿还信用卡本金'. $res['principal'] .'元及至实际清偿之日止的利息、滞纳金、费用（暂计至'. $date_of_entry .'，尚欠利息'. $res['interest'] .'元';
		$str2 = $res['late_fee'] > 0 ?'、滞纳金'. $res['late_fee'] .'元' : '';
		$str2_2 = $res['other2'] > 0 ?'、违约金'. $res['other2'] .'元' : '';
		$str3 = $res['staging_fee_balance'] > 0 ? '、分期手续费'. $res['staging_fee_balance'] .'元' : '';
		$str5 = $res['increment1'] > 0 ? '、“用卡无忧”增值服务费'. $res['increment1'] .'元' : '';
		$str6 = $res['increment2'] > 0 ? '、“信用保障”增值服务费'. $res['increment2'] .'元' : '';
		$str6_2 = $res['poundage_fee'] > 0 ? '、取现手续费'. $res['poundage_fee'] .'元' : '';
		$str6_3 = $res['transfer_accounts_fee'] > 0 ? '、转账手续费'. $res['transfer_accounts_fee'] .'元' : '';
		$str6_4 = $res['query_fee'] > 0 ? '、银联境外账户查询手续费'. $res['query_fee'] .'元' : '';
		$str4 = $res['annual_fee'] > 0 ? '、年费'. $res['annual_fee'] .'元' : '';
		$str4_2 = $res['loss_fee'] > 0 ? '、挂失手续费'. $res['loss_fee'] .'元' : '';
		$str4_3 = $res['card_fee'] > 0 ? '、补发新卡工本费'. $res['card_fee'] .'元' : '';

		$str7 = '，此后的利息按日利率万分之五计算），上述款项暂合计'. $res['total_fee'] .'元 ；';

		$strA1 = $str1.$str2.$str2_2.$str3.$str5.$str6.$str6_2.$str6_3.$str6_4.$str4.$str4_2.$str4_3.$str7;
		$section->addText($strA1,'FangSong14pt', 'Normal',null);

		$section->addText(
			'	2、被告承担本案诉讼费。',
		 	'FangSong14pt', 'Normal',
		 	null
		);

		// $section->addText(''); // 空行
		
		$section->addText(
			'	事实和理由:',
		 	'FangSong14pt', 'Normal',
		 	null
		);

		$section->addText(
			'	'. $date_of_open .'，被告为申领信用卡向原告提交信用卡申请表，确认愿意遵守该申请表后附领用合约的各项规则。其后，原告经审核向被告发放信用卡（卡号：'. $res['card_sn'] .'），被告持卡进行透支消费。截至'. $date_of_entry .'，被告尚欠款项及各项收费标准如下表：',
		 	'FangSong14pt', 'Normal',
		 	null
		);

		$section->addText(''); // 空行
		// $section->addText(''); // 空行
		// $section->addText(''); // 空行


		$tableStyle = array(
		    'borderSize'  => 6,
		    'cantSplit'  => false,    
		    'tblHeader'  => false,    
		    'cellMargin'  => 50
		);
		$firstRowStyle = array('align' => 'center');
		$phpWord->addTableStyle('myTable', $tableStyle, $firstRowStyle);
		$table = $section->addTable('myTable');

		$styleCell = array('align' => 'center','valign' => 'center');
		$fontStyle1 = array('bold' => true, 'align' => 'center','valign' => 'center');
		$fontStyle = array('align' => 'center','valign' => 'center');


		$table->addRow(200);
		$table->addCell(1000, $styleCell)->addText('序号', $fontStyle1,$styleCell);
		$table->addCell(1600, $styleCell)->addText('项目', $fontStyle1,$styleCell);
		$table->addCell(1500, $styleCell)->addText('金额', $fontStyle1,$styleCell);
		$table->addCell(4900, $styleCell)->addText('收费标准', $fontStyle1,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('1', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('本金', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['principal'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('————', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('2', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('利息', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['interest'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《领用合约》：日万分之五，按月计收复利', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('3', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('滞纳金', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['late_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《领用合约》：按最低还款额未还部分的5%收取，最低人民币10元/1美元', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('4', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('违约金', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['other2'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《交通银行太平洋信用卡服务收费名录》SGRKD002项：按最低还款额未还部分的5%收取，最低人民币10元/1美元，2017年1月1日开始计收', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('5', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('“用卡无忧”增值服务费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['increment1'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《交通银行太平洋信用卡服务收费名录》SGRKD0015项：12元/季度', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('6', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('“信用保障”增值服务费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['increment2'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《交通银行太平洋信用卡服务收费名录》SGRKD0016项：9元/季度', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('7', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('分期手续费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['staging_fee_balance'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《交通银行太平洋信用卡服务收费名录》SGRKD019-020项', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('8', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('取现手续费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['poundage_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《领用合约》：信用额度境内人民币取现按交易金额的1%收取，最低每笔人民币10元', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('9', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('超限费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['overlimit_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《领用合约》：按超额部分的5%，最低人民币5元或1美元', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('10', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('挂失手续费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['loss_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《交通银行太平洋信用卡服务收费名录》 SGRKD005项：人民币50元', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('11', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('年费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['annual_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《交通银行太平洋信用卡服务收费名录》SGRKD001项', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('12', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('补发新卡工本费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['card_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《交通银行太平洋信用卡服务收费名录》SGRKD003项：人民币15元，加急处理加收人民币35元', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('13', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('转账手续费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['transfer_accounts_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《领用合约》：信用额度按交易金额的1%收取，最低每笔人民币10元，最高500元', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('14', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('“刷得保”增值服务费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['increment3'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('《交通银行太平洋信用卡服务收费名录》SGRKD0018项：18元/季度/户', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('15', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('银联境外账户查询手续费', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['query_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('详见《交通银行太平洋信用卡服务收费名录》SGRKD014项：4元/笔', $fontStyle,$styleCell);

		$table->addRow(400);
		$table->addCell(1000, $styleCell)->addText('16', $fontStyle,$styleCell);
		$table->addCell(1600, $styleCell)->addText('总计', $fontStyle,$styleCell);
		$table->addCell(1500, $styleCell)->addText($res['total_fee'], $fontStyle,$styleCell);
		$table->addCell(4900, $styleCell)->addText('————', $fontStyle,$styleCell);

		// $section->addText(''); // 空行

		$section->addText(
			'	以上事实有信用卡申领文件、领用合约、交易明细等证据证明。被告逾期还款的行为已构成违约，严重损害原告利益。根据有关信用卡文件的约定，原告有权要求被告立即偿还欠款本金及至实际清偿日止的利息、滞纳金、违约金和相关费用。',
		 	'FangSong12pt', 'Small'
		);

		$section->addText(
			'	请求法院查清事实,判如所请，维护原告的合法权益。',
		 	'FangSong12pt', 'Small'
		);

		$section->addText(
			'	此致',
		 	'FangSong12pt', 'Small'
		);

		$section->addText(
			'佛山市禅城区人民法院',
		 	'FangSong12pt', 'Small'
		);

		$section->addText(
			'                                    具状人: 交通银行股份有限公司佛山分行',
		 	'FangSong12pt', 'Small'
		);

		$section->addText(
			'                                                      '.$res['last_date'],
		 	'FangSong12pt', 'Small'
		);
		


		/*****************************************************************/

		$file = $res['name'].'.docx';
		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		// $xmlWriter->save("./static/word/".$file);
		// var_dump($xmlWriter);die;
		// $downurl = 'abc';
		// $this->success($file,'/static/template/cost_tpl.xlsx');
		// var_dump($abc);die;
		$this->_output($phpWord, $file);

	}


	private function _output($phpWord, $file) {
		header("Content-Description: File Transfer");
		header('Content-Disposition: attachment; filename="' . $file . '"');
		header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
		header('Content-Transfer-Encoding: binary');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Expires: 0');
		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$xmlWriter->save("php://output");
	}

	



}
