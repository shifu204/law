<?php
//------------------------
// 老赖验证器
//-------------------------

namespace app\common\validate;

use think\Validate;

class Deadbeat extends Validate
{
    protected $rule = [

        'transfer_accounts_fee|转账手续费'          => 'float',
        'query_fee|银联境外账户查询手续费'           => 'float',
        'increment1|用卡无忧增值服务费'              => 'float',
        'increment2|信用保障增值服务费'              => 'float',
        'increment3|刷得宝增值服务费'                => 'float',
        'identity_details|身份证详情'               => 'max:256',
        'last_date|文档最后中文时间'                => 'max:64',
    ];



}