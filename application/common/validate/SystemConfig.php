<?php
//------------------------
// 系统参数验证器
//-------------------------

namespace app\common\validate;

use think\Validate;

class SystemConfig extends Validate
{
    protected $rule = [
        // wx_api
        'wechat_appid|APPID'                 => 'require|max:18',
        'wechat_appsecret|appsecret'         => 'require|max:32',
        'wechat_token|令牌'                  => 'require|max:256',
        'wechat_encodingaeskey|消息加密密钥' => 'max:43',
        // wx_pay
        'wechat_mch_id|商户ID'       => 'require|max:10',
        'wechat_partnerkey|商户密钥' => 'require|max:32',
        // smtp 
        'smtp_server|smtp服务器' => 'require|max:128',
        'smtp_port|smtp端口'     => 'require|max:32',
        'smtp_user|smtp用户'     => 'require|max:64',
        'smtp_password|smtp密码' => 'require|max:64',
    ];

    protected $message = [
        'wechat_appid.require'      => '请输入APPID',
        'wechat_appsecret.require'  => '请输入wechat_appsecret',
        'wechat_token.require'      => '请输入令牌',
        'wechat_mch_id.require'     => '请输入商户id',
        'wechat_partnerkey.require' => '请输入商户密钥',

    ];


    protected $scene = [
        'wx_api'  => ['wechat_appid','wechat_appsecret','wechat_token','wechat_encodingaeskey'],
        'wx_pay'  => ['wechat_mch_id','wechat_partnerkey'],
        'smtp'    => ['smtp_server','smtp_port','smtp_user','smtp_password'],
    ];
}