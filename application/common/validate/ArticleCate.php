<?php
//------------------------
// 资讯分类验证器
//-------------------------

namespace app\common\validate;

use think\Validate;

class ArticleCate extends Validate
{
    protected $rule = [
        'parent_id|上级id'       => 'require|number',
        'cate_name|分类名称'     => 'require|max:32|unique:article_cate',
       
    ];

    protected $message = [
        'cate_name.require' => '请输入资讯分类名称',
    ];




}