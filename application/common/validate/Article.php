<?php
//------------------------
// 资讯分类验证器
//-------------------------

namespace app\common\validate;

use think\Validate;

class Article extends Validate
{
    protected $rule = [
        'cate_id|上级id'       => 'require|number',
        'new_title|资讯标题'     => 'require|unique:article',
        'abstract|摘要'        => 'require|unique:article',
        'author|作者'          =>'require|unique:article',
        'source|来源'         =>'require|unique:article',
        'sort_num|排序值'         =>'require|number|max:50',
        'content|资讯内容'    =>'require|unique:article',
    ];

    protected $message = [
        
    ];




}