<?php
//------------------------
// 用户组管理验证器
//-------------------------

namespace app\common\validate;

use think\Validate;

class AuthGroup extends Validate
{
    protected $rule = [
        'title|用户组名'         => 'require|max:20|unique:auth_group',
        'status|状态'            => 'require|number',
        'is_delete|能否删除'      => 'require|number',

    ];

    protected $scene = [
        'add'  => ['title','status','isdelete'],
        'edit'  => ['title','status','isdelete'],
    ];

}