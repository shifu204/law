<?php
//------------------------
// 后台用户验证器
//-------------------------

namespace app\common\validate;

use think\Validate;

class AdminUser extends Validate
{
    protected $rule = [
        'username|帐号'             => 'require|max:20|unique:admin_user',
        'password|密码'             => 'require|confirm:confirm_password',
        'confirm_password|确认密码' => 'require|confirm:password',
        'status|状态'               => 'number',
        'is_delete|能否删除'        => 'number',
        'realname|真实姓名'         => 'max:10',
        'email|邮箱'                => 'email',
        'mobile|电话'               => 'number|max:20',
        'remark|备注'               => 'max:250',
    ];

    protected $message = [
        'username.require'         => '请输入用户名',
        'username.unique'          => '用户名已存在',
        'password.confirm'         => '两次输入密码不一致',
        'confirm_password.confirm' => '两次输入密码不一致',
    ];


    protected $scene = [
        'add'  => ['username', 'password','confirm_password','status','is_delete'],
        'editCommon'   => ['username','status','is_delete'],
        'editDetailed' => ['realname','email','mobile','remark'],
        'editPassword' => ['password','confirm_password'],
    ];
}