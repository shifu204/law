<?php
//------------------------
// 权限管理验证器
//-------------------------

namespace app\common\validate;

use think\Validate;

class AuthRule extends Validate
{
    protected $rule = [
        'pid|上级id'             => 'require|number',
        'name|规则名称'          => 'require|max:80|unique:auth_rule',
        'title|规则描述'         => 'require|max:20|unique:auth_rule',
        'status|状态'            => 'require|number',
        'condition|附加规则描述' => 'max:100',
    ];

    protected $message  =   [
        'pid.number' => '父级权限pid必须为数字',
        'name.unique:auth_rule' => '权限名称以存在',
        'title.unique:auth_rule' => '权限描述以存在',

    ];

    protected $scene = [
        'add'  => ['pid', 'name', 'title','status','condition'],
        'edit'  => ['pid', 'name', 'title','status','condition'],
    ];
}