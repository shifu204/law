/*
Navicat MySQL Data Transfer

Source Server         : 开发机3
Source Server Version : 50725
Source Host           : 192.168.1.219:3306
Source Database       : law

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2019-05-05 16:44:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for platform_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `platform_admin_user`;
CREATE TABLE `platform_admin_user` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(32) NOT NULL DEFAULT '' COMMENT '帐号名称',
  `realname` varchar(255) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '密码',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_login_ip` char(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `login_count` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '总登录次数',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `mobile` char(11) NOT NULL DEFAULT '' COMMENT '电话',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '50' COMMENT '状态',
  `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '能否删除',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `usernamepassword` (`username`,`password`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of platform_admin_user
-- ----------------------------
INSERT INTO `platform_admin_user` VALUES ('1', 'admin', '', '4aa185ccb3cc7fbcc627d47ee94b1ecd', '1557045572', '192.168.1.219', '236', '', '', '', '1', '0', '0', '0');
INSERT INTO `platform_admin_user` VALUES ('14', 'test1', '', '314319e0a853228a54e2bdeaa6649efc', '1492145665', '192.168.118.128', '11', '', '', '', '1', '1', '1491815982', '0');
INSERT INTO `platform_admin_user` VALUES ('12', 'admin2', '', '54fd32508052a403b038bb73ad8c40b6', '1491807102', '', '0', '', '', '', '1', '0', '1491807102', '0');
INSERT INTO `platform_admin_user` VALUES ('13', 'admin3', '', '54fd32508052a403b038bb73ad8c40b6', '1491815968', '', '0', '', '', '', '1', '1', '1491815968', '0');
INSERT INTO `platform_admin_user` VALUES ('15', 'test2', '', '314319e0a853228a54e2bdeaa6649efc', '1491816000', '', '0', '', '', '', '1', '1', '1491816000', '0');
INSERT INTO `platform_admin_user` VALUES ('16', 'test3', '', '314319e0a853228a54e2bdeaa6649efc', '1492145699', '192.168.118.128', '9', '', '', '', '1', '1', '1491816036', '0');
INSERT INTO `platform_admin_user` VALUES ('17', 'test', '', '314319e0a853228a54e2bdeaa6649efc', '1491816497', '192.168.118.128', '2', '', '', '', '1', '1', '1491816051', '0');
INSERT INTO `platform_admin_user` VALUES ('18', 'test4', '', '314319e0a853228a54e2bdeaa6649efc', '1491816285', '', '0', '', '', '', '1', '1', '1491816285', '0');
INSERT INTO `platform_admin_user` VALUES ('23', '2', '', '3151a54002005ff3b6bf63a3b4187721', '1491893362', '', '0', '', '', '', '1', '1', '1491893362', '0');
INSERT INTO `platform_admin_user` VALUES ('24', '3', '', '0fabc1503553b00b2e5c2de3032606a9', '1491893377', '', '0', '', '', '', '1', '1', '1491893377', '0');

-- ----------------------------
-- Table structure for platform_article
-- ----------------------------
DROP TABLE IF EXISTS `platform_article`;
CREATE TABLE `platform_article` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `new_title` varchar(128) NOT NULL COMMENT '资讯标题',
  `cate_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '咨询分类id',
  `abstract` varchar(512) NOT NULL COMMENT '摘要',
  `author` varchar(64) NOT NULL COMMENT '作者',
  `source` varchar(32) NOT NULL COMMENT '来源',
  `logo` varchar(150) NOT NULL COMMENT 'logo原图',
  `content` longtext COMMENT '内容',
  `is_examine` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '1通过审核，0没通过',
  `addtime` int(10) unsigned NOT NULL COMMENT '添加时间',
  `see` int(11) NOT NULL DEFAULT '0' COMMENT '阅读数',
  `sort_num` tinyint(3) unsigned NOT NULL DEFAULT '50' COMMENT '排序值',
  `is_on` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否上架,1:上架,0:下架',
  PRIMARY KEY (`id`),
  KEY `cate_id` (`cate_id`) USING BTREE,
  KEY `addtime` (`addtime`) USING BTREE,
  KEY `sort_num` (`sort_num`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of platform_article
-- ----------------------------

-- ----------------------------
-- Table structure for platform_article_cate
-- ----------------------------
DROP TABLE IF EXISTS `platform_article_cate`;
CREATE TABLE `platform_article_cate` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(64) NOT NULL COMMENT '资讯分类名称',
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类id:0代表顶级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of platform_article_cate
-- ----------------------------
INSERT INTO `platform_article_cate` VALUES ('5', 'A & Q', '0');
INSERT INTO `platform_article_cate` VALUES ('6', '商品质量问题', '7');
INSERT INTO `platform_article_cate` VALUES ('7', '商品相关问题', '5');

-- ----------------------------
-- Table structure for platform_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `platform_auth_group`;
CREATE TABLE `platform_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '组名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：0为关闭 1为开启',
  `is_delete` tinyint(1) NOT NULL DEFAULT '1' COMMENT '能否删除',
  `rules` varchar(4096) NOT NULL DEFAULT '' COMMENT '规则id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of platform_auth_group
-- ----------------------------
INSERT INTO `platform_auth_group` VALUES ('1', '管理员', '1', '0', '1,2,3,4,5,6,7,8,9,10,11,12,17,19');
INSERT INTO `platform_auth_group` VALUES ('9', '测试组1', '1', '0', '1,2,3,4,5,6,7,8,9,10,11,12,34,24,27,25,26,28,29,30,31,32,33,35,36,37,38,39,40,41,42,43,44,49,45,46,47,48');
INSERT INTO `platform_auth_group` VALUES ('10', '测试组2', '1', '0', '1,3,4,6,10,11,12,24,25,26,29,33,35,36,37,40,41,45,46');

-- ----------------------------
-- Table structure for platform_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `platform_auth_group_access`;
CREATE TABLE `platform_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL COMMENT '会员id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of platform_auth_group_access
-- ----------------------------
INSERT INTO `platform_auth_group_access` VALUES ('14', '9');
INSERT INTO `platform_auth_group_access` VALUES ('15', '9');
INSERT INTO `platform_auth_group_access` VALUES ('16', '10');
INSERT INTO `platform_auth_group_access` VALUES ('17', '9');
INSERT INTO `platform_auth_group_access` VALUES ('17', '10');
INSERT INTO `platform_auth_group_access` VALUES ('18', '10');

-- ----------------------------
-- Table structure for platform_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `platform_auth_rule`;
CREATE TABLE `platform_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pid` mediumint(8) NOT NULL DEFAULT '0' COMMENT '上级id',
  `name` varchar(80) NOT NULL DEFAULT '' COMMENT '认证规则',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '规则描述',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'tinyint类型',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：0为关闭 1为开启',
  `condition` varchar(100) NOT NULL DEFAULT '' COMMENT '附加条件',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of platform_auth_rule
-- ----------------------------
INSERT INTO `platform_auth_rule` VALUES ('1', '0', 'admin/AuthRule/index', '权限列表', '1', '1', '{login_count}>1');
INSERT INTO `platform_auth_rule` VALUES ('2', '1', 'admin/AuthRule/add', '添加权限页面', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('3', '1', 'admin/AuthRule/edit', '编辑权限页面', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('4', '1', 'admin/AuthRule/addProc', '添加/编辑权限操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('5', '1', 'admin/AuthRule/delete', '删除权限', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('6', '0', 'admin/AdminUser/index', '用户列表', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('7', '6', 'admin/AdminUser/addProc', '用户添加操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('8', '6', 'admin/AdminUser/edit', '用户编辑页面', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('9', '6', 'admin/AdminUser/editProc', '用户编辑操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('10', '6', 'admin/AdminUser/delete', '用户删除操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('11', '6', 'admin/AdminUser/batchProc', '用户批量操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('12', '6', 'admin/AdminUser/add', '用户添加页面', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('27', '24', 'admin/AuthGroup/batchProc', '用户组批量操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('24', '0', 'admin/AuthGroup/index', '用户组列表', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('25', '24', 'admin/AuthGroup/add', '添加用户组页面', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('26', '24', 'admin/AuthGroup/edit', '编辑用户组页面', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('28', '24', 'admin/AuthGroup/saveAuth', '设置权限操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('29', '24', 'admin/AuthGroup/setAuth', '设置权限页面', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('30', '24', 'admin/AuthGroup/delete', '删除用户组', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('31', '24', 'admin/AuthGroup/addProc', '添加用户组操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('32', '24', 'admin/AuthGroup/saveUser', '设置成员操作', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('33', '24', 'admin/AuthGroup/setUser', '设置成员页面', '1', '1', '');
INSERT INTO `platform_auth_rule` VALUES ('34', '6', 'admin/AdminUser/detail', '用户详情', '1', '1', '');

-- ----------------------------
-- Table structure for platform_deadbeat
-- ----------------------------
DROP TABLE IF EXISTS `platform_deadbeat`;
CREATE TABLE `platform_deadbeat` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '姓名',
  `address` varchar(256) NOT NULL DEFAULT '' COMMENT '邮寄地址',
  `identity_sn` varchar(32) NOT NULL DEFAULT '' COMMENT '身份证号',
  `identity_details` varchar(256) NOT NULL DEFAULT '' COMMENT '身份证详情',
  `phone` varchar(32) NOT NULL DEFAULT '' COMMENT '手机',
  `telephone` varchar(32) NOT NULL DEFAULT '' COMMENT '固话',
  `card_sn` varchar(32) NOT NULL DEFAULT '' COMMENT '卡号',
  `principal` decimal(10,2) DEFAULT '0.00' COMMENT '本金',
  `interest` decimal(10,2) DEFAULT '0.00' COMMENT '利息',
  `staging_fee_balance` decimal(10,2) DEFAULT '0.00' COMMENT '分期手续费余额',
  `transfer_accounts_fee` decimal(10,2) DEFAULT '0.00' COMMENT '转账手续费',
  `query_fee` decimal(10,2) DEFAULT '0.00' COMMENT '银联境外账户查询手续费',
  `increment1` decimal(10,2) DEFAULT '0.00' COMMENT '增值服务费1，用卡无忧',
  `increment2` decimal(10,2) DEFAULT '0.00' COMMENT '增值服务费2，信用保障',
  `increment3` decimal(10,2) DEFAULT '0.00' COMMENT '增值服务费3，刷得宝',
  `late_fee` decimal(10,2) DEFAULT '0.00' COMMENT '滞纳金',
  `overlimit_fee` decimal(10,2) DEFAULT '0.00' COMMENT '超限费',
  `loss_fee` decimal(10,2) DEFAULT '0.00' COMMENT '挂失费',
  `annual_fee` decimal(10,2) DEFAULT '0.00' COMMENT '年费',
  `card_fee` decimal(10,2) DEFAULT '0.00' COMMENT '补卡费',
  `poundage_fee` decimal(10,2) DEFAULT '0.00' COMMENT '取现手续费',
  `other_fee` decimal(10,2) DEFAULT '0.00' COMMENT '正确其他费用',
  `total_fee` decimal(10,2) DEFAULT '0.00' COMMENT '最新余额',
  `date_of_open` varchar(32) DEFAULT '' COMMENT '开户日期',
  `permanent_address` varchar(255) DEFAULT NULL COMMENT '住址',
  `nation` varchar(64) DEFAULT NULL COMMENT '国籍',
  `date_of_entry` varchar(32) DEFAULT '' COMMENT '入账日期',
  `birthday` varchar(64) DEFAULT NULL COMMENT '生日',
  `sex` varchar(64) DEFAULT NULL COMMENT '性别',
  `last_date` varchar(64) DEFAULT NULL COMMENT '文档最后的中文日期',
  `other2` decimal(10,2) DEFAULT '0.00' COMMENT '违约金',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of platform_deadbeat
-- ----------------------------
INSERT INTO `platform_deadbeat` VALUES ('125', '黄结仪', '广东省佛山市南海区罗村新湖一路１２号２０３房', '440682197805064740', '', '13535611022', '075786449922', '5218993710236046', '11998.90', '8333.90', '258.90', '77.00', '88.00', '24.00', '17.69', '0.00', '495.02', '22.00', '33.00', '44.00', '55.00', '66.00', '0.00', '21524.41', '2014-09-03', '广东省佛山市南海区罗村街道务庄荣星村大街15号', '汉', '2019-3-12', '1956年6月24日', '女', '二零一八年九月三日', '11.00');
INSERT INTO `platform_deadbeat` VALUES ('126', '李晓殷', '广东省茂名市茂南区高凉南路富丽苑 11 号天天德高装饰建材有限公司', '440923198401294022', '', '13927596227', '06682152161', '5218993710242861', '11973.85', '8345.32', '186.74', '77.00', '88.00', '12.00', '9.00', '0.00', '617.00', '22.00', '33.00', '44.00', '55.00', '66.00', '0.00', '21539.91', '2009-06-30', '广东省茂名市茂港区羊角镇石曹黑石岭村83号', '汉族', '2019-3-13', '1956年6月25日', '女', '二零一八年九月三日', '11.00');
INSERT INTO `platform_deadbeat` VALUES ('124', '陈雄毅', '广东省佛山市南海区桂城南新三路２号', '44092319840312027X', '', '13539320115', '075763327062', '5218993718182911', '11912.72', '8812.12', '0.00', '77.00', '88.00', '12.00', '9.00', '0.00', '437.73', '22.00', '33.00', '44.00', '55.00', '66.00', '0.00', '21579.57', '2013-06-17', '广东省电白县电城镇白蕉兴平中村32号', '汉', '2019-3-11', '1956年6月23日', '男', '二零一八年九月三日', '11.00');

-- ----------------------------
-- Table structure for platform_file
-- ----------------------------
DROP TABLE IF EXISTS `platform_file`;
CREATE TABLE `platform_file` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '文件名',
  `origin_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '原文件名',
  `size` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '文件大小',
  `ext` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '后缀名',
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'file' COMMENT '文件类型',
  `md5` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'md5值',
  `absolute_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '文件绝对路径',
  `relative_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '文件相对路径',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`file_id`),
  KEY `type` (`type`),
  KEY `ext` (`ext`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='上传文件列表';

-- ----------------------------
-- Records of platform_file
-- ----------------------------
INSERT INTO `platform_file` VALUES ('63', '20180528152749263751244619.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'b11c9a3a556f8d722651c1ad91e9d17d', '/data/www/law/public/Uploads/file/2018/05/20180528152749263751244619.xls', './Uploads/file/2018/05/20180528152749263751244619.xls', '1527492637', '1527492637');
INSERT INTO `platform_file` VALUES ('64', '20180528152749271256215818.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'b11c9a3a556f8d722651c1ad91e9d17d', '/data/www/law/public/Uploads/file/2018/05/20180528152749271256215818.xls', './Uploads/file/2018/05/20180528152749271256215818.xls', '1527492712', '1527492712');
INSERT INTO `platform_file` VALUES ('65', '20180528152749290685131290.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'b11c9a3a556f8d722651c1ad91e9d17d', '/data/www/law/public/Uploads/file/2018/05/20180528152749290685131290.xls', './Uploads/file/2018/05/20180528152749290685131290.xls', '1527492906', '1527492906');
INSERT INTO `platform_file` VALUES ('66', '20180528152749320537488619.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'b11c9a3a556f8d722651c1ad91e9d17d', '/data/www/law/public/Uploads/file/2018/05/20180528152749320537488619.xls', './Uploads/file/2018/05/20180528152749320537488619.xls', '1527493205', '1527493205');
INSERT INTO `platform_file` VALUES ('67', '20180528152749332200443608.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'b11c9a3a556f8d722651c1ad91e9d17d', '/data/www/law/public/Uploads/file/2018/05/20180528152749332200443608.xls', './Uploads/file/2018/05/20180528152749332200443608.xls', '1527493322', '1527493322');
INSERT INTO `platform_file` VALUES ('68', '20180528152749342031647842.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'b11c9a3a556f8d722651c1ad91e9d17d', '/data/www/law/public/Uploads/file/2018/05/20180528152749342031647842.xls', './Uploads/file/2018/05/20180528152749342031647842.xls', '1527493420', '1527493420');
INSERT INTO `platform_file` VALUES ('69', '20180528152749352581753250.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'b11c9a3a556f8d722651c1ad91e9d17d', '/data/www/law/public/Uploads/file/2018/05/20180528152749352581753250.xls', './Uploads/file/2018/05/20180528152749352581753250.xls', '1527493525', '1527493525');
INSERT INTO `platform_file` VALUES ('70', '20180528152749365942805963.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'b11c9a3a556f8d722651c1ad91e9d17d', '/data/www/law/public/Uploads/file/2018/05/20180528152749365942805963.xls', './Uploads/file/2018/05/20180528152749365942805963.xls', '1527493659', '1527493659');
INSERT INTO `platform_file` VALUES ('71', '20180528152749394778792361.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'c7c3a2b03a4780ca044403d09eab26b3', '/data/www/law/public/Uploads/file/2018/05/20180528152749394778792361.xls', './Uploads/file/2018/05/20180528152749394778792361.xls', '1527493947', '1527493947');
INSERT INTO `platform_file` VALUES ('72', '20180528152749402272346821.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'c7c3a2b03a4780ca044403d09eab26b3', '/data/www/law/public/Uploads/file/2018/05/20180528152749402272346821.xls', './Uploads/file/2018/05/20180528152749402272346821.xls', '1527494022', '1527494022');
INSERT INTO `platform_file` VALUES ('73', '20180528152749413602560875.xlsx', 'device_template (2).xlsx', '8624', 'xlsx', 'file', '06975461b876e8035398ecf49e954036', '/data/www/law/public/Uploads/file/2018/05/20180528152749413602560875.xlsx', './Uploads/file/2018/05/20180528152749413602560875.xlsx', '1527494136', '1527494136');
INSERT INTO `platform_file` VALUES ('74', '20180528152749476204203570.xlsx', 'device_template (2).xlsx', '8624', 'xlsx', 'file', '06975461b876e8035398ecf49e954036', '/data/www/law/public/Uploads/file/2018/05/20180528152749476204203570.xlsx', './Uploads/file/2018/05/20180528152749476204203570.xlsx', '1527494762', '1527494762');
INSERT INTO `platform_file` VALUES ('75', '20180528152749477638062907.xls', '交行催收的联系方式.xls', '49664', 'xls', 'file', 'c7c3a2b03a4780ca044403d09eab26b3', '/data/www/law/public/Uploads/file/2018/05/20180528152749477638062907.xls', './Uploads/file/2018/05/20180528152749477638062907.xls', '1527494776', '1527494776');
INSERT INTO `platform_file` VALUES ('76', '20180528152749518921917775.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749518921917775.xls', './Uploads/file/2018/05/20180528152749518921917775.xls', '1527495189', '1527495189');
INSERT INTO `platform_file` VALUES ('77', '20180528152749526344437520.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749526344437520.xls', './Uploads/file/2018/05/20180528152749526344437520.xls', '1527495263', '1527495263');
INSERT INTO `platform_file` VALUES ('78', '20180528152749532262112813.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749532262112813.xls', './Uploads/file/2018/05/20180528152749532262112813.xls', '1527495322', '1527495322');
INSERT INTO `platform_file` VALUES ('79', '20180528152749548712435323.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749548712435323.xls', './Uploads/file/2018/05/20180528152749548712435323.xls', '1527495487', '1527495487');
INSERT INTO `platform_file` VALUES ('80', '20180528152749571467502938.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749571467502938.xls', './Uploads/file/2018/05/20180528152749571467502938.xls', '1527495714', '1527495714');
INSERT INTO `platform_file` VALUES ('81', '20180528152749577159618656.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749577159618656.xls', './Uploads/file/2018/05/20180528152749577159618656.xls', '1527495771', '1527495771');
INSERT INTO `platform_file` VALUES ('82', '20180528152749579929425390.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749579929425390.xls', './Uploads/file/2018/05/20180528152749579929425390.xls', '1527495799', '1527495799');
INSERT INTO `platform_file` VALUES ('83', '20180528152749593653210846.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749593653210846.xls', './Uploads/file/2018/05/20180528152749593653210846.xls', '1527495936', '1527495936');
INSERT INTO `platform_file` VALUES ('84', '20180528152749596151107578.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749596151107578.xls', './Uploads/file/2018/05/20180528152749596151107578.xls', '1527495961', '1527495961');
INSERT INTO `platform_file` VALUES ('85', '20180528152749602037109196.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749602037109196.xls', './Uploads/file/2018/05/20180528152749602037109196.xls', '1527496020', '1527496020');
INSERT INTO `platform_file` VALUES ('86', '20180528152749609742971189.xls', '交行催收的联系方式.xls', '45568', 'xls', 'file', 'b25a9125cff918919a6dd3c9ee632a4a', '/data/www/law/public/Uploads/file/2018/05/20180528152749609742971189.xls', './Uploads/file/2018/05/20180528152749609742971189.xls', '1527496097', '1527496097');
INSERT INTO `platform_file` VALUES ('87', '20180528152749761321541154.xlsx', 'template.xlsx', '10682', 'xlsx', 'file', '5a1762a449dda7f83401bfbab26902c7', '/data/www/law/public/Uploads/file/2018/05/20180528152749761321541154.xlsx', './Uploads/file/2018/05/20180528152749761321541154.xlsx', '1527497613', '1527497613');
INSERT INTO `platform_file` VALUES ('88', '20180528152749823485721446.xlsx', 'template.xlsx', '10682', 'xlsx', 'file', '5a1762a449dda7f83401bfbab26902c7', '/data/www/law/public/Uploads/file/2018/05/20180528152749823485721446.xlsx', './Uploads/file/2018/05/20180528152749823485721446.xlsx', '1527498234', '1527498234');
INSERT INTO `platform_file` VALUES ('89', '20180528152749827936697485.xlsx', 'template.xlsx', '10682', 'xlsx', 'file', '5a1762a449dda7f83401bfbab26902c7', '/data/www/law/public/Uploads/file/2018/05/20180528152749827936697485.xlsx', './Uploads/file/2018/05/20180528152749827936697485.xlsx', '1527498279', '1527498279');
INSERT INTO `platform_file` VALUES ('90', '20180528152750429813896924.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152750429813896924.xlsx', './Uploads/file/2018/05/20180528152750429813896924.xlsx', '1527504298', '1527504298');
INSERT INTO `platform_file` VALUES ('91', '20180528152750998490962652.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152750998490962652.xlsx', './Uploads/file/2018/05/20180528152750998490962652.xlsx', '1527509984', '1527509984');
INSERT INTO `platform_file` VALUES ('92', '20180528152751994216742223.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152751994216742223.xlsx', './Uploads/file/2018/05/20180528152751994216742223.xlsx', '1527519942', '1527519942');
INSERT INTO `platform_file` VALUES ('93', '20180528152752003366547594.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752003366547594.xlsx', './Uploads/file/2018/05/20180528152752003366547594.xlsx', '1527520033', '1527520033');
INSERT INTO `platform_file` VALUES ('94', '20180528152752055450262009.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752055450262009.xlsx', './Uploads/file/2018/05/20180528152752055450262009.xlsx', '1527520554', '1527520554');
INSERT INTO `platform_file` VALUES ('95', '20180528152752060053266396.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752060053266396.xlsx', './Uploads/file/2018/05/20180528152752060053266396.xlsx', '1527520600', '1527520600');
INSERT INTO `platform_file` VALUES ('96', '20180528152752067001019704.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752067001019704.xlsx', './Uploads/file/2018/05/20180528152752067001019704.xlsx', '1527520670', '1527520670');
INSERT INTO `platform_file` VALUES ('97', '20180528152752141768546791.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752141768546791.xlsx', './Uploads/file/2018/05/20180528152752141768546791.xlsx', '1527521417', '1527521417');
INSERT INTO `platform_file` VALUES ('98', '20180528152752143429368331.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752143429368331.xlsx', './Uploads/file/2018/05/20180528152752143429368331.xlsx', '1527521434', '1527521434');
INSERT INTO `platform_file` VALUES ('99', '20180528152752172405227860.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752172405227860.xlsx', './Uploads/file/2018/05/20180528152752172405227860.xlsx', '1527521724', '1527521724');
INSERT INTO `platform_file` VALUES ('100', '20180528152752197478854618.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752197478854618.xlsx', './Uploads/file/2018/05/20180528152752197478854618.xlsx', '1527521974', '1527521974');
INSERT INTO `platform_file` VALUES ('101', '20180528152752313819262633.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180528152752313819262633.xlsx', './Uploads/file/2018/05/20180528152752313819262633.xlsx', '1527523138', '1527523138');
INSERT INTO `platform_file` VALUES ('102', '20180529152752365708816601.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180529152752365708816601.xlsx', './Uploads/file/2018/05/20180529152752365708816601.xlsx', '1527523657', '1527523657');
INSERT INTO `platform_file` VALUES ('103', '20180529152752371288816680.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180529152752371288816680.xlsx', './Uploads/file/2018/05/20180529152752371288816680.xlsx', '1527523712', '1527523712');
INSERT INTO `platform_file` VALUES ('104', '20180529152752378778413029.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180529152752378778413029.xlsx', './Uploads/file/2018/05/20180529152752378778413029.xlsx', '1527523787', '1527523787');
INSERT INTO `platform_file` VALUES ('105', '20180529152752380629407026.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180529152752380629407026.xlsx', './Uploads/file/2018/05/20180529152752380629407026.xlsx', '1527523806', '1527523806');
INSERT INTO `platform_file` VALUES ('106', '20180529152752396588102637.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180529152752396588102637.xlsx', './Uploads/file/2018/05/20180529152752396588102637.xlsx', '1527523965', '1527523965');
INSERT INTO `platform_file` VALUES ('107', '20180529152752413307307500.xls', 'template2.xls', '45568', 'xls', 'file', '4490ec2f11de80560f5ff99230d1c6eb', '/data/www/law/public/Uploads/file/2018/05/20180529152752413307307500.xls', './Uploads/file/2018/05/20180529152752413307307500.xls', '1527524133', '1527524133');
INSERT INTO `platform_file` VALUES ('108', '20180529152752448178425011.xls', 'template2.xls', '45568', 'xls', 'file', '4490ec2f11de80560f5ff99230d1c6eb', '/data/www/law/public/Uploads/file/2018/05/20180529152752448178425011.xls', './Uploads/file/2018/05/20180529152752448178425011.xls', '1527524481', '1527524481');
INSERT INTO `platform_file` VALUES ('109', '20180529152752456900030710.xls', 'template2.xls', '45568', 'xls', 'file', '4490ec2f11de80560f5ff99230d1c6eb', '/data/www/law/public/Uploads/file/2018/05/20180529152752456900030710.xls', './Uploads/file/2018/05/20180529152752456900030710.xls', '1527524568', '1527524568');
INSERT INTO `platform_file` VALUES ('110', '20180529152752916330874328.xls', 'template2.xls', '45568', 'xls', 'file', '1e3b78e503d715dac7b9ae355aa4355a', '/data/www/law/public/Uploads/file/2018/05/20180529152752916330874328.xls', './Uploads/file/2018/05/20180529152752916330874328.xls', '1527529163', '1527529163');
INSERT INTO `platform_file` VALUES ('111', '20180529152752931477528154.xlsx', 'template (1).xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180529152752931477528154.xlsx', './Uploads/file/2018/05/20180529152752931477528154.xlsx', '1527529314', '1527529314');
INSERT INTO `platform_file` VALUES ('112', '20180529152752933302351243.xls', 'template2.xls', '45568', 'xls', 'file', '1e3b78e503d715dac7b9ae355aa4355a', '/data/www/law/public/Uploads/file/2018/05/20180529152752933302351243.xls', './Uploads/file/2018/05/20180529152752933302351243.xls', '1527529333', '1527529333');
INSERT INTO `platform_file` VALUES ('113', '20180529152752947933412534.xlsx', 'template1.xlsx', '10840', 'xlsx', 'file', 'ca487e6aed58798bb420d85d0b02c07c', '/data/www/law/public/Uploads/file/2018/05/20180529152752947933412534.xlsx', './Uploads/file/2018/05/20180529152752947933412534.xlsx', '1527529479', '1527529479');
INSERT INTO `platform_file` VALUES ('114', '20180529152752949349012190.xls', 'template2.xls', '45568', 'xls', 'file', '4490ec2f11de80560f5ff99230d1c6eb', '/data/www/law/public/Uploads/file/2018/05/20180529152752949349012190.xls', './Uploads/file/2018/05/20180529152752949349012190.xls', '1527529493', '1527529493');
INSERT INTO `platform_file` VALUES ('115', '20180529152753001321069988.xls', 'template2.xls', '45568', 'xls', 'file', '4490ec2f11de80560f5ff99230d1c6eb', '/data/www/law/public/Uploads/file/2018/05/20180529152753001321069988.xls', './Uploads/file/2018/05/20180529152753001321069988.xls', '1527530013', '1527530013');
INSERT INTO `platform_file` VALUES ('116', '20180601152781759144291246.xlsx', 'template3.xlsx', '9365', 'xlsx', 'file', '74b72c59e4c2bce87344998a02cceb68', '/data/www/law/public/Uploads/file/2018/06/20180601152781759144291246.xlsx', './Uploads/file/2018/06/20180601152781759144291246.xlsx', '1527817591', '1527817591');
INSERT INTO `platform_file` VALUES ('117', '20180601152781761342942008.xlsx', 'template3.xlsx', '9365', 'xlsx', 'file', '74b72c59e4c2bce87344998a02cceb68', '/data/www/law/public/Uploads/file/2018/06/20180601152781761342942008.xlsx', './Uploads/file/2018/06/20180601152781761342942008.xlsx', '1527817613', '1527817613');
INSERT INTO `platform_file` VALUES ('118', '20180601152781773498694457.xlsx', 'template3.xlsx', '9333', 'xlsx', 'file', 'a793ce9f8dc9ca5c249934c80114babf', '/data/www/law/public/Uploads/file/2018/06/20180601152781773498694457.xlsx', './Uploads/file/2018/06/20180601152781773498694457.xlsx', '1527817734', '1527817734');
INSERT INTO `platform_file` VALUES ('119', '20180601152781779878791019.xlsx', 'template3.xlsx', '9333', 'xlsx', 'file', 'a793ce9f8dc9ca5c249934c80114babf', '/data/www/law/public/Uploads/file/2018/06/20180601152781779878791019.xlsx', './Uploads/file/2018/06/20180601152781779878791019.xlsx', '1527817798', '1527817798');
INSERT INTO `platform_file` VALUES ('120', '20180601152781793694036294.xlsx', 'template3.xlsx', '9382', 'xlsx', 'file', '59fb014294288541f718b84bfab1160e', '/data/www/law/public/Uploads/file/2018/06/20180601152781793694036294.xlsx', './Uploads/file/2018/06/20180601152781793694036294.xlsx', '1527817936', '1527817936');
INSERT INTO `platform_file` VALUES ('121', '20180601152781795240501319.xlsx', 'template3.xlsx', '9382', 'xlsx', 'file', '59fb014294288541f718b84bfab1160e', '/data/www/law/public/Uploads/file/2018/06/20180601152781795240501319.xlsx', './Uploads/file/2018/06/20180601152781795240501319.xlsx', '1527817952', '1527817952');
INSERT INTO `platform_file` VALUES ('122', '20190417155549151200559420.xls', 'identity_tpl (1).xls', '22016', 'xls', 'file', 'd24b3f68c8295cbe54736c60a0fa6d27', '/data/www/law/public/Uploads/file/2019/04/20190417155549151200559420.xls', './Uploads/file/2019/04/20190417155549151200559420.xls', '1555491512', '1555491512');
INSERT INTO `platform_file` VALUES ('123', '20190417155549153305685917.xls', 'identity_tpl (1).xls', '22016', 'xls', 'file', 'd24b3f68c8295cbe54736c60a0fa6d27', '/data/www/law/public/Uploads/file/2019/04/20190417155549153305685917.xls', './Uploads/file/2019/04/20190417155549153305685917.xls', '1555491533', '1555491533');
INSERT INTO `platform_file` VALUES ('124', '20190417155549155953312443.xlsx', 'cost_tpl (1).xlsx', '9735', 'xlsx', 'file', '0658759f1ec450f45665dc3971403137', '/data/www/law/public/Uploads/file/2019/04/20190417155549155953312443.xlsx', './Uploads/file/2019/04/20190417155549155953312443.xlsx', '1555491559', '1555491559');
INSERT INTO `platform_file` VALUES ('125', '20190417155549158683106217.xls', 'identity_tpl (1).xls', '22016', 'xls', 'file', 'd24b3f68c8295cbe54736c60a0fa6d27', '/data/www/law/public/Uploads/file/2019/04/20190417155549158683106217.xls', './Uploads/file/2019/04/20190417155549158683106217.xls', '1555491586', '1555491586');
INSERT INTO `platform_file` VALUES ('126', '20190417155549236803378890.xlsx', '拆分表模板.xlsx', '9735', 'xlsx', 'file', '0658759f1ec450f45665dc3971403137', '/data/www/law/public/Uploads/file/2019/04/20190417155549236803378890.xlsx', './Uploads/file/2019/04/20190417155549236803378890.xlsx', '1555492368', '1555492368');
INSERT INTO `platform_file` VALUES ('127', '20190417155549237820959788.xls', '身份信息模板.xls', '22016', 'xls', 'file', 'ac82d1145f3c5e5b2b3127e5c92e466a', '/data/www/law/public/Uploads/file/2019/04/20190417155549237820959788.xls', './Uploads/file/2019/04/20190417155549237820959788.xls', '1555492378', '1555492378');
INSERT INTO `platform_file` VALUES ('128', '20190505155702607890137207.xls', '数据拆分表.xls', '39936', 'xls', 'file', 'd1868db023b3e3b1540b8fda1a403e54', '/data/www/law/public/Uploads/file/2019/05/20190505155702607890137207.xls', './Uploads/file/2019/05/20190505155702607890137207.xls', '1557026078', '1557026078');
INSERT INTO `platform_file` VALUES ('129', '20190505155702613369236129.xlsx', 'cost_tpl (1).xlsx', '9735', 'xlsx', 'file', '0658759f1ec450f45665dc3971403137', '/data/www/law/public/Uploads/file/2019/05/20190505155702613369236129.xlsx', './Uploads/file/2019/05/20190505155702613369236129.xlsx', '1557026133', '1557026133');
INSERT INTO `platform_file` VALUES ('130', '20190505155702617150023316.xls', 'identity_tpl (1).xls', '22016', 'xls', 'file', 'fd699877057cd98957c6b3346748e15e', '/data/www/law/public/Uploads/file/2019/05/20190505155702617150023316.xls', './Uploads/file/2019/05/20190505155702617150023316.xls', '1557026171', '1557026171');
INSERT INTO `platform_file` VALUES ('131', '20190505155704115577506338.xls', '数据拆分表.xls', '23040', 'xls', 'file', 'd0195ed49aca78491e3b673fb681d4d9', '/data/www/law/public/Uploads/file/2019/05/20190505155704115577506338.xls', './Uploads/file/2019/05/20190505155704115577506338.xls', '1557041155', '1557041155');
INSERT INTO `platform_file` VALUES ('132', '20190505155704127821374848.xls', '数据拆分表.xls', '23040', 'xls', 'file', 'd0195ed49aca78491e3b673fb681d4d9', '/data/www/law/public/Uploads/file/2019/05/20190505155704127821374848.xls', './Uploads/file/2019/05/20190505155704127821374848.xls', '1557041278', '1557041278');
INSERT INTO `platform_file` VALUES ('133', '20190505155704130911679878.xls', '数据拆分表.xls', '23040', 'xls', 'file', 'd0195ed49aca78491e3b673fb681d4d9', '/data/www/law/public/Uploads/file/2019/05/20190505155704130911679878.xls', './Uploads/file/2019/05/20190505155704130911679878.xls', '1557041309', '1557041309');
INSERT INTO `platform_file` VALUES ('134', '20190505155704133194567226.xls', '数据拆分表.xls', '23040', 'xls', 'file', 'd0195ed49aca78491e3b673fb681d4d9', '/data/www/law/public/Uploads/file/2019/05/20190505155704133194567226.xls', './Uploads/file/2019/05/20190505155704133194567226.xls', '1557041331', '1557041331');
INSERT INTO `platform_file` VALUES ('135', '20190505155704179069044624.xls', '数据拆分表.xls', '23040', 'xls', 'file', '8f6b6aa41f4078e31f2835c34004fa31', '/data/www/law/public/Uploads/file/2019/05/20190505155704179069044624.xls', './Uploads/file/2019/05/20190505155704179069044624.xls', '1557041790', '1557041790');
INSERT INTO `platform_file` VALUES ('136', '20190505155704256111276594.xls', 'cost_tpl.xls', '23040', 'xls', 'file', '55cf2b5713ae14315faece3fcc29590f', '/data/www/law/public/Uploads/file/2019/05/20190505155704256111276594.xls', './Uploads/file/2019/05/20190505155704256111276594.xls', '1557042561', '1557042561');
INSERT INTO `platform_file` VALUES ('137', '20190505155704257112911223.xls', 'identity_tpl.xls', '22016', 'xls', 'file', 'f125456b075cc3dd3b7f61ca183dbbe3', '/data/www/law/public/Uploads/file/2019/05/20190505155704257112911223.xls', './Uploads/file/2019/05/20190505155704257112911223.xls', '1557042571', '1557042571');
INSERT INTO `platform_file` VALUES ('138', '20190505155704258331853200.xls', 'identity_tpl.xls', '22016', 'xls', 'file', 'f125456b075cc3dd3b7f61ca183dbbe3', '/data/www/law/public/Uploads/file/2019/05/20190505155704258331853200.xls', './Uploads/file/2019/05/20190505155704258331853200.xls', '1557042583', '1557042583');
INSERT INTO `platform_file` VALUES ('139', '20190505155704261594801224.xls', 'identity_tpl.xls', '22016', 'xls', 'file', 'f125456b075cc3dd3b7f61ca183dbbe3', '/data/www/law/public/Uploads/file/2019/05/20190505155704261594801224.xls', './Uploads/file/2019/05/20190505155704261594801224.xls', '1557042615', '1557042615');
INSERT INTO `platform_file` VALUES ('140', '20190505155704262906410237.xls', 'identity_tpl.xls', '22016', 'xls', 'file', 'f125456b075cc3dd3b7f61ca183dbbe3', '/data/www/law/public/Uploads/file/2019/05/20190505155704262906410237.xls', './Uploads/file/2019/05/20190505155704262906410237.xls', '1557042629', '1557042629');
INSERT INTO `platform_file` VALUES ('141', '20190505155704265707436310.xls', 'identity_tpl.xls', '22016', 'xls', 'file', 'f125456b075cc3dd3b7f61ca183dbbe3', '/data/www/law/public/Uploads/file/2019/05/20190505155704265707436310.xls', './Uploads/file/2019/05/20190505155704265707436310.xls', '1557042657', '1557042657');
INSERT INTO `platform_file` VALUES ('142', '20190505155704329104085347.xls', 'identity_tpl.xls', '22528', 'xls', 'file', '7473e3b4064475c5bfc820e8fd6f77a6', '/data/www/law/public/Uploads/file/2019/05/20190505155704329104085347.xls', './Uploads/file/2019/05/20190505155704329104085347.xls', '1557043291', '1557043291');
INSERT INTO `platform_file` VALUES ('143', '20190505155704330783492322.xls', 'identity_tpl.xls', '22528', 'xls', 'file', '7473e3b4064475c5bfc820e8fd6f77a6', '/data/www/law/public/Uploads/file/2019/05/20190505155704330783492322.xls', './Uploads/file/2019/05/20190505155704330783492322.xls', '1557043307', '1557043307');
INSERT INTO `platform_file` VALUES ('144', '20190505155704521528858392.xls', 'cost_tpl.xls', '23040', 'xls', 'file', '4c43e23334f8cf8bb5c2f61d5d4e2ec4', '/data/www/law/public/Uploads/file/2019/05/20190505155704521528858392.xls', './Uploads/file/2019/05/20190505155704521528858392.xls', '1557045215', '1557045215');
INSERT INTO `platform_file` VALUES ('145', '20190505155704522389764709.xls', 'identity_tpl.xls', '22016', 'xls', 'file', 'fd699877057cd98957c6b3346748e15e', '/data/www/law/public/Uploads/file/2019/05/20190505155704522389764709.xls', './Uploads/file/2019/05/20190505155704522389764709.xls', '1557045223', '1557045223');
INSERT INTO `platform_file` VALUES ('146', '20190505155704544876115912.xls', 'identity_tpl.xls', '22016', 'xls', 'file', 'fd699877057cd98957c6b3346748e15e', '/data/www/law/public/Uploads/file/2019/05/20190505155704544876115912.xls', './Uploads/file/2019/05/20190505155704544876115912.xls', '1557045448', '1557045448');
INSERT INTO `platform_file` VALUES ('147', '20190505155704560178021462.xls', 'identity_tpl (1).xls', '22528', 'xls', 'file', '43111609e442de304833b6da60d0f2bb', '/data/www/law/public/Uploads/file/2019/05/20190505155704560178021462.xls', './Uploads/file/2019/05/20190505155704560178021462.xls', '1557045601', '1557045601');
INSERT INTO `platform_file` VALUES ('148', '20190505155704583145371088.xls', 'cost_tpl.xls', '23040', 'xls', 'file', '27d4f8b5bf588bb8a980b04e0ab26482', '/data/www/law/public/Uploads/file/2019/05/20190505155704583145371088.xls', './Uploads/file/2019/05/20190505155704583145371088.xls', '1557045831', '1557045831');
INSERT INTO `platform_file` VALUES ('149', '20190505155704583906718556.xls', 'identity_tpl.xls', '22528', 'xls', 'file', 'bb3b32dcddc98c3598868bd387761ecb', '/data/www/law/public/Uploads/file/2019/05/20190505155704583906718556.xls', './Uploads/file/2019/05/20190505155704583906718556.xls', '1557045839', '1557045839');

-- ----------------------------
-- Table structure for platform_system_config
-- ----------------------------
DROP TABLE IF EXISTS `platform_system_config`;
CREATE TABLE `platform_system_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '配置键',
  `value` text COMMENT '配置值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of platform_system_config
-- ----------------------------
INSERT INTO `platform_system_config` VALUES ('1', 'wechat_api_parameter', '{\"id\":\"1\",\"wechat_appid\":\"wxa86153a91420728b\",\"wechat_appsecret\":\"7744f2080386dceadc106bfd45ae2963\",\"wechat_token\":\"syhuo\",\"wechat_encodingaeskey\":\"\"}');
INSERT INTO `platform_system_config` VALUES ('2', 'wechat_pay_parameter', '{\"id\":\"2\",\"wechat_mch_id\":\"1259951301\",\"wechat_partnerkey\":\"telpo111111111111111111111111111\"}');
INSERT INTO `platform_system_config` VALUES ('3', 'smtp_parameter', '{\"id\":\"3\",\"smtp_server\":\"smtp.163.com\",\"smtp_port\":\"25\",\"smtp_user\":\"shifu204@163.com\",\"smtp_password\":\"lijiahua@1\",\"smtp_secure\":\"TSL\"}');
